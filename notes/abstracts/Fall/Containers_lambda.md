---
title: Контейнеры и лямбды
tags: cpp, abstract, 2sem
---

# Последовательные контейнеры

## array (c++11) 
	static contiguous array

*Обертка* над обычным статичным массивом. Итератор является `RandomAccessIterator`.
Есть следующие методы:
```cpp=
#include <array>

int main () {
  std::array<double> my_array = {-1, 2, 3.14, 2.67, 5.0};
  my_array.at(4);  // access specified element with bounds checking
  my_array[10];  //	access specified element
  my_array.front();  // access the first element
  my_array.back();  // access the last element
  my_array.begin(); // returns an iterator to the beginning
  my_array.cend();  // returns an iterator to the end
  my_array.rbegin();  // returns a reverse iterator to the beginning
  my_array.empty();  //	checks whether the container is empty
  my_array.size();  // returns the number of elements
  my_array.max_size();  // returns the maximum possible number of elements
}
```

## vector
	dynamic contiguous array

Массив, который можно расширить без особых усилий. Итератор является `RandomAccessIterator`. Вектор в STL разделен на две части: буффер, который работает с памятью и обертка над буфером, которая предоставляет интерфейс работы с контейнером.
В вектор можно добавлять элементы и в какой-то момент размер буффера будет мал для входящих элементов. В таком случае вызывается специальный метод, который увеличивает размер буфера.
В методы работы над буфером также относится `shrink_to_fit`, который уменьшает размер буфера и освобождает часть неиспользуемой памяти.
Примеры использования вектора:
```cpp=
#include <iostream>
#include <vector>

int main()
{
    std::vector<int> g1;
 
    for (int i = 1; i <= 5; i++)
        g1.push_back(i);
 
    cout << "Size : " << g1.size();
    cout << "\nCapacity : " << g1.capacity();
    cout << "\nMax_Size : " << g1.max_size();
 
    // resizes the vector size to 4
    g1.resize(4);
 
    // prints the vector size after resize()
    cout << "\nSize : " << g1.size();
 
    // checks if the vector is empty or not
    if (g1.empty() == false)
        cout << "\nVector is not empty";
    else
        cout << "\nVector is empty";
 
    // Shrinks the vector
    g1.shrink_to_fit();
    cout << "\nVector elements are: ";
    for (auto it = g1.begin(); it != g1.end(); it++)
        cout << *it << " ";
 
    return 0;
}
```

## deque
	double-ended queue

Двухсторонняя очередь, которая позволяет за амортизированное O(1) обращаться к элементам. Итератор является `RandomAccessIterator`
![](https://i.imgur.com/JL4lmkM.png)
Возможность быстро обращаться к любому элементу заложена во внутренней структуре очереди. Внутри скрывается массив массивов, который через двойные индексы позволяет обращаться к нужному элементу. При добавлении нового элемента он появляется слева или справа.
Добавление нового элемента в очередь дешевле аналогичного добавления в векторе. В очереди не требуется дополнительно копировать элементы, они будут добавлены с новым массивом.
Пример использования двухсторонней очереди:
```cpp=
#include <deque>
#include <iostream>
 
void showdq(deque<int> g)
{
    std::deque<int>::iterator it;
    for (it = g.begin(); it != g.end(); ++it)
        cout << '\t' << *it;
    cout << '\n';
}
  
int main()
{
    std::deque<int> gquiz;
    gquiz.push_back(10);
    gquiz.push_front(20);
    gquiz.push_back(30);
    gquiz.push_front(15);
    cout << "The deque gquiz is : ";
    showdq(gquiz);
  
    cout << "\ngquiz.size() : " << gquiz.size();
    cout << "\ngquiz.max_size() : " << gquiz.max_size();
  
    cout << "\ngquiz.at(2) : " << gquiz.at(2);
    cout << "\ngquiz.front() : " << gquiz.front();
    cout << "\ngquiz.back() : " << gquiz.back();
  
    cout << "\ngquiz.pop_front() : ";
    gquiz.pop_front();
    showdq(gquiz);
  
    cout << "\ngquiz.pop_back() : ";
    gquiz.pop_back();
    showdq(gquiz);
  
    return 0;
}
```

## list
	doubly-linked list

Список имеет привычную структуру, еще узнаваемую с прошлого семестра. Есть узлы, которые хранят информацию и указатель на соседний узел.
По списку можно итерироваться в одну сторону (`ForvardIteration`).
Основные методы списка
```cpp=
#include <list>

int main() {
  std::list<int> my_list = {1, 2, 3, 4, 5};
  std::list<int>::iterator my_iterator = my_list.end();
  my_list.insert(my_iterator, 12);  // Add element to the end
                                    // Last element is NIL (special ptr_end)
  my_list.push_back(13);  // Add element to the end
  my_list.pop_back();  // pop last element
  my_list.pop_front();  // pop first element
  my_list.clear();  // Remove all nodes
  // etc...
}
```
Структура списка STL имеет особенность. Последний элемент указывает на особую ноду NIL. Это позволяет реализовать вставку элемента в конец и не инвалидации итератора на список.
![](https://i.imgur.com/TIhDTnD.png)

## forward_list (c++11)
	singly-linked list

Двусвязный список почти не отличается от односвязного. Итераторы являются `BidirectionalIterator`, что позволяет перемещаться по списку в обе стороны. Как и в односвязном списке, есть специальный элемент NIL, на который ссылаются непривязанные указатели первого (prev) и последнего (next) элементов.
Методы в точности повторяют методы `std::list`, возможно почти всюду.


# Ассоциативные контейнеры

## set

Множество (обычно все равно называют "сет") является ассоциативным контейнером, хранящим уникальные объекты в отсортированном порядке. Сортировка производится через шаблонный параметр `Compare`, т.е. подразумевается, что для объектов определен оператор `<`. Поиск, удаление и вставка работают за `O(log(n))`. В своей основе имеют красно-черные деревья. 

Основные функции для работы с множествами:
```cpp=
Iterators
iterator begin()  noexcept; // return iterator to the beginning
const_iterator cbegin() noexcept; // since c++11; returns const iterator
iterator end()    noexcept; // return iterator to the end
const_iterator cend()   noexcept; // since c++11; returns const iterator
// reverse iterators 
// ...

Capacity
bool empty() noexcept; // checks whether the container is empty
// some other methods...

Modifiers
void clear() noexcept; // clears the contents
... insert(element);   // inserts elements or nodes (since C++17)
iterator erase(element); // erases elements and returns iterator after the last removed
// some other methods...

Lookup
size_t count() noexcept; // returns the number of elements matching specific key
iterator find() // finds element with specific key and returns iterator to it element
```

Пример использования:
```cpp=
#include <iostream>
#include <iterator>
#include <set>

using namespace std;
 
int main()
{
    // empty set container with 
    // value_type == int
    // compare    == std::greater<value_type>
    set<int, greater<int> > s1;
 
    // insert elements in random order
    s1.insert(40);
    s1.insert(30);
    s1.insert(60);
    s1.insert(20);
    s1.insert(50);
 
    // only one 50 will be added to the set
    s1.insert(50);
    s1.insert(10);
 
    // printing set s1
    for (auto&& item : s1) {
        cout << *item << " ";
    }
    cout << endl;
 
    // assigning the elements from s1 to s2
    set<int> s2(s1.begin(), s1.end());
 
    // print all elements of the set s2
    cout << "\nThe set s2 after assign from s1 is : \n";
    for (auto&& item : s2) {
        cout << *item << " ";
    }
    cout << endl;
 
    // remove all elements up to 30 in s2
    cout << "\ns2 after removal of elements less than 30 "
            ":\n";
    s2.erase(s2.begin(), s2.find(30));
    for (auto&& item : s2) {
        cout << *item << " ";
    }
 
    // remove element with value 50 in s2
    int num;
    num = s2.erase(50);
    cout << "\ns2.erase(50) : ";
    cout << num << " removed\n";
    for (auto&& item : s2) {
        cout << *itr << " ";
    }
    cout << endl;

    return 0;
}
```

Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/set)
## map

Словарь (обычно тоже называют просто "мапа") также является сортированным ассоциативным контейнером, хранящим пары ключ-значение. Сортировка происходит по ключу, т.е. для ключа должен быть определен оператор `<`. Функция сортировки таже подается через параметры шаблона. Поиск, удаление и вставка работают за `O(log(n))`. Обычно также реализованы на основе красно-черных деревьев

Основные функции для работы со словарями:
```cpp=
Element access
// access specified element with bounds checking
// throws out_of_bound exceptions if no element
value_type& at(key); 
// access specified element without bounds checking
// if no element creates one
value_type& operator[key]

Iterators
// 	returns an iterator to the beginning
iterator begin()
const_iterator cbegin()
// returns an iterator to the end
iterator end() 
const_iterator cend()
// some other iterator methods...

Capacity
// checks whether the container is empty
size_t empty();
// returns the number of elements	
size_t size();
	
Modifiers
// clears the contents
void clear();
// inserts elements or nodes (since C++17)	
... insert(value);
// erases elements
... erase(key);

Lookup
// returns the number of elements matching specific key
size_t count(key);
// finds element with specific key	
iterator find(key)
```

Пример использования:
```cpp=
#include <iostream>
#include <iterator>
#include <map>
using namespace std;
  
int main()
{
  
    // empty map container
    //  key  value
    //   |     |
    //   v     v
    map<int, int> gquiz1;
  
    // insert elements in random order
    gquiz1.insert(pair<int, int>(1, 40));
    gquiz1.insert(pair<int, int>(2, 30));
    gquiz1.insert(pair<int, int>(3, 60));
    gquiz1.insert(pair<int, int>(4, 20));
    gquiz1.insert(pair<int, int>(5, 50));
    gquiz1.insert(pair<int, int>(6, 50));
      
    gquiz1[7] = 10; // another way of inserting a value in a map
     
  
    // printing map gquiz1
    cout << "\nThe map gquiz1 is : \n";
    cout << "\tKEY\tELEMENT\n";
    // since c++17
    for (auto&& [key, value] : gquiz1) {
        cout << '\t' << key << '\t' << value
             << '\n';
    }
    cout << endl;
    // since c++11
    for (auto&& item : gquiz1) {
        cout << '\t' << item->first << '\t' << item->second
             << '\n';
    }
    cout << endl;
    // one more way using iterators
    for (itr = gquiz2.begin(); itr != gquiz2.end(); ++itr) {
        cout << '\t' << itr->first << '\t' << itr->second
             << '\n';
    }
    cout << endl;
  
    // assigning the elements from gquiz1 to gquiz2
    map<int, int> gquiz2(gquiz1.begin(), gquiz1.end());
  
    // print all elements of the map gquiz2
    cout << "\nThe map gquiz2 after"
         << " assign from gquiz1 is : \n";
    cout << "\tKEY\tELEMENT\n";
    for (auto&& [key, value] : gquiz2) {
        cout << '\t' << key << '\t' << value
             << '\n';
    }
    cout << endl;
  
    // remove all elements up to
    // element with key=3 in gquiz2
    cout << "\ngquiz2 after removal of"
            " elements less than key=3 : \n";
    cout << "\tKEY\tELEMENT\n";
    gquiz2.erase(gquiz2.begin(), gquiz2.find(3));
    for (auto&& [key, value] : gquiz2) {
        cout << '\t' << key << '\t' << value
             << '\n';
    }
    cout << endl;
  
    // remove all elements with key = 4
    int num;
    num = gquiz2.erase(4);
    cout << "\ngquiz2.erase(4) : ";
    cout << num << " removed \n";
    cout << "\tKEY\tELEMENT\n";
     for (auto&& [key, value] : gquiz2) {
        cout << '\t' << key << '\t' << value
             << '\n';
    }
    cout << endl;
    return 0;
}
```

Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/map)
## multiset

Мультисет является ассоциативным контейнером, который содержит объекты в отсортированном порядке. В отличие от обычного множетсва позволяет хранить много одинаковых элементов.

По сравнению с обычным множеством добавляются новые методы, а именно:
```cpp=
// returns range of elements matching a specific key
//  start_iter  end_iter
//      |         |
//      v         v
pair<iterator, iterator> equal_range(key);
	
// returns an iterator to the first element not less than the given key
iterator lower_bound(key);
// returns an iterator to the first element greater than the given key 	
iterator upper_bound(key);	
```

Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/multiset)

## multimap
Мультисловарь является ассоциативным контейнером, который содержит пары ключ-значение в отсортированном по ключу порядке. В отличие от обычного множетсва позволяет хранить много элементов с одинаковым ключом.

По сравнению с обычным словарем добавляются новые методы, а именно:
```cpp=
// returns range of elements matching a specific key
//  start_iter  end_iter
//      |         |
//      v         v
pair<iterator, iterator> equal_range(key);
	
// returns an iterator to the first element not less than the given key
iterator lower_bound(key);
// returns an iterator to the first element greater than the given key 	
iterator upper_bound(key);	
```

Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/multimap)
# Неупорядоченные ассоциативные контейнеры

## unordered_set (C++11)

Неупорядоченное множество является ассоциативным контейнером, содержашим уникальные объекты. Поиск, вставка и удаление выполняются за амортизированное `O(1)`.

Внутреннее представление неупорядоченного множества представляет собой хэш-таблицу со своими характерными плюсами и минусами.

Основные методы:
```cpp=
Iterators
iterator begin()  noexcept; // return iterator to the beginning
const_iterator cbegin() noexcept; // since c++11; returns const iterator
iterator end()    noexcept; // return iterator to the end
const_iterator cend()   noexcept; // since c++11; returns const iterator
// reverse iterators 
// ...

Capacity
bool empty() noexcept; // checks whether the container is empty
// some other methods...

Modifiers
void clear() noexcept; // clears the contents
... insert(element);   // inserts elements or nodes (since C++17)
iterator erase(element); // erases elements and returns iterator after the last removed
// some other methods...

Lookup
size_t count() noexcept; // returns the number of elements matching specific key
iterator find() // finds element with specific key and returns iterator to it element

Hash policy
// returns average number of elements per bucket
float load_factor();
// reserves at least the specified number of buckets and regenerates the hash table	
void rehash(count);
// reserves space for at least the specified number of elements and regenerates the hash table
void reserve(count);

Observers
// returns function used to hash the keys
hasher hash_function();
```

Пример использования:
```cpp=
#include <bits/stdc++.h>
using namespace std;
  
int main()
{
    // declaring set for storing string data-type
    unordered_set <string> stringSet ;
  
    // inserting various string, same string will be stored
    // once in set
  
    stringSet.insert("code") ;
    stringSet.insert("in") ;
    stringSet.insert("c++") ;
    stringSet.insert("is") ;
    stringSet.insert("fast") ;
  
    string key = "slow" ;
  
    //  find returns end iterator if key is not found,
    //  else it returns iterator to that key
  
    if (stringSet.find(key) == stringSet.end())
        cout << key << " not found" << endl << endl ;
    else
        cout << "Found " << key << endl << endl ;
  
    key = "c++";
    if (stringSet.find(key) == stringSet.end())
        cout << key << " not found\n" ;
    else
        cout << "Found " << key << endl ;
  
    // now iterating over whole set and printing its
    // content
    cout << "\nAll elements : ";
    unordered_set<string> :: iterator itr;
    for (itr = stringSet.begin(); itr != stringSet.end(); itr++)
        cout << (*itr) << endl;
}
```

Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/unordered_set)
## unordered_map (C++11)

Неупорядоченный словарь является контейнером, содержащим пары ключ-значение с уникальными ключами.
Поиск, вставка и удаление выполняются за амортизированное `O(1)`.

Внутреннее представление неупорядоченного множества представляет собой хэш-таблицу со своими характерными плюсами и минусами.

Функции аналогичны `unordered_set` и `map`.

Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/unordered_map)
## unordered_multiset (C++11)
Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/unordered_multiset)
## unordered_multimap (C++11)
Подробнее можно [прочитать тут](https://en.cppreference.com/w/cpp/container/unordered_multimap) 
# Лямбды

С появлением 11-го стандарта появились лямбда-выражения (они могут быть знакомы вам из Python). Лямбда-выражение позволяет на место объявить функцию и воспользоваться ею.

## Идея

Определять отдельную функцию только для того, чтобы передать её один раз в качестве аргумента в другую функцию - занятие утомительное. Поэтому С++ предлагает запись чего-то, что действует как функция, прямо в позиции агрумента или временной переменной. Например,
```cpp=
std::sort(vr.begin(), vr.end(), 
         [ ] (const std::string& a, const std::string& b)
          {
              return a < b;
          });
```

## Синтаксис

Минимальный вид лямбда-выражения
```cpp=
[](){};
```
а теперь подробнее:
- `[<args>]` -- захват внешних переменных. Синтаксис, который позволяет захватить внешние переменные и использовать их внутри лямбда-выражения;
- `(args...)` -- список аргументов;
- `{ /* code */ }` -- тело выражения (логика исполнения)

Рассмотрим более насущный пример компоратора:
```cpp=
struct Complex {
    int re = 0, im = 0;
};

int main() {
    auto comparator = [](const Complex& lhs, const Complex& rhs) {
        auto abs = [](const Complex& val) {
                        return val.re * val.re + val.im * val.im;
                    };
        if (abs(lhs) < abs(rhs)) {
            return true;
        }
        return false;
    };
}
```

Подробнее можно почитать [тут](https://en.cppreference.com/w/cpp/language/lambda)

### std::function

В качестве возвращаемого типа лямбда-выражения может использоваться `std::function` из библиотеки `<functional>`:
```cpp=
#include <iostream>
#include <functional>

struct Complex {
    int re = 0, im = 0;
};

void print_abs(const std::function<double(const Complex&)> abs, const Complex& value) {
    std::cout << abs(value) << std::endl;
}

int main() {
    auto abs = [](const Complex& val) {
                    return val.re * val.re + val.im * val.im;
                };
  
}
```
Это позволяет явным образом перекидывать лямбда-выражение в какую-либо еще структуру.

`auto glambda = []<class T>(T a, auto&& b) { return a < b; };`

### Захват по значению

В лямбда-выражение можно передавать параметры из локального окружения. Это возможно благодаря первому аргументу `[<args>]`. Аргументы можно захватывать по значению следующим способом:
```cpp=
#include <iostream>
#include <functional>

int main() {
    int  counter = 0;
    std::cout << counter << '\n';
    std::function<void()> change_counter = [counter]() mutable { ++counter; };
    change_counter();
    std::cout << counter << '\n';
    return 0;
}
```
`mutable` -- ключевое слово, которое позволяет нарушить условие константности переменных и позволяет изменить переменную `counter`. В общем случае, захваченные таким образом переменные являются константными.

### Захват по ссылке

Процедуру изменения `counter` можно осуществить и в другом виде. Для этого требуется передать `counter` по ссылке:
```cpp=
#include <iostream>
#include <functional>

int main() {
    int  counter = 0;
    std::cout << counter << '\n';
    std::function<void()> change_counter = [&counter]() { ++counter; };
    change_counter();
    std::cout << counter << '\n';
    return 0;
}
```

Перечислять все захватываемые переменные бывает сложно и трудно. Для этого изобретен упрощающий синтаксис передачи всех видимых локальных объектов:

```cpp=
#include <iostream>
#include <functional>

int main() {
    int  counter = 0;
    std::cout << counter << '\n';
    std::function<void()> change_counter1 = [=]() mutable { ++counter; };
    std::function<void()> change_counter2 = [&]() { ++counter; };
    change_counter1();
    std::cout << counter << '\n';
    change_counter2();
    std::cout << counter << '\n';
    return 0;
}
```
# Задачи

- [Кролики](https://hackmd.io/XvJg0SK-S6u2497DF40ztg)
- [RandomizedSet](https://hackmd.io/aZ_5hBloRZmKuA0Vb9Y6UQ)
- [Большая половина](https://hackmd.io/XMtapuQaQ5iZJ8kRBetQ6Q)
- [Сумма двух](https://hackmd.io/scCOUXFQTVKG-cbitZysKw)
- [Дубликаты](https://hackmd.io/q_r-kmfDShy-y71FJwINwQ)
- [Повторы в ДНК](https://hackmd.io/h02Xjv3ERJ6_FUhByUl1ng) 
- [Распределяющая шляпа](https://hackmd.io/1xGKhksXRFCR45QmtzYHFQ)
- [Разделение](https://hackmd.io/t5o6g5i7QomGDm2iNquYsA)