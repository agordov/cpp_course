---
title: Множественное наследование, виртуальные функции и таблицы
tags: cpp, abstract, 2sem
---

# Наследование

Наследование -- механизм переиспользования и расширения функциональности имеющегося класса, от общего к специализированному.

Наследование в С++ от одного класса может быть трех типов: `private`, `public`, `protected`. Данные модификаторы позволяют указать возможность обращения к методам и полям класса-родителя вне класса-наследника.

```cpp=
class Base {
  public:
    int a = 0;

    void f() {
      std:: cout <<"Base\n";
    }
};

class DerivedPublic: public Base {  // наследование может быть public, private, protected
  public:
    int b = 1;
    void g() {
      std::cout << "DerivedPublic " << a << "\n";
    }
};

class DerivedPrivate: private Base {
  public:
    int b = 1;
    void g() {
      std::cout << "DerivedPrivate " << a << "\n";
    }
};

int main() {
  DerivedPublic derived_public = DerivedPublic();
  DerivedPrivate derived_private = DerivedPrivate();
  
  derived_public.f();  // OK, наследование типа public
  derived_public.g(); // OK, метод публичный
  
  derived_private.f();  // NOT OK, наследование типа private
  derived_private.g(); // OK, метод публичный
  
  return 0;
}
```

Запрет на использование полей класса `Base` в `DerivedPrivate` следует из `DerivedPrivate`. Именно  `DerivedPrivate` запрещает публичное использование методов и полей вне класса `DerivedPrivate`.

Стоит упомянуть, что функцию `int main` можно сделать `friend` (но так делать плохо) в классе `DerivedPrivate`. Тогда внутри `main` обращаение к полям и методам `Base` из `DerivedPrivate` будет разрешен.

Если функцию `int main` объявить `friend` в классе `Base`, то для `DerivedPrivate` ничего не изменится. **`friend` не наследуется!**

## Модификатор доступа protected

`private` позволяет предоставлять доступ к полям и методам внутри класса и друзьям (`friend`).

`public` разрешает доступ к полям и методам не только внутри класса, но и снаружи (друзья теряют смысл в таком случае).

А вот `protected` особый тип доступа, который позволяет получить доступ к полям и методам внутри класса-наследника, но ограничивает доступ к таким полям и методам снаружи. Пример:
```cpp=
class Base {
  protected:
    int a = 0;

    void f() {
      std:: cout <<"Base\n";
    }
};

class Derived: private Base {
  public:
    int b = 1;
    void g() {
      std::cout << "DerivedPublic " << a << "\n";
    }
};

class SubDerived: public Derived {
  public:
    int c = 2;
    void h() {
      std::cout << "SubDerived " << a << "\n";
    }
};

int main() {
  Derived derived = Derived();
  SubDerived sub_derived = SubDerived();

  derived.g();  // OK, 
  derived.f();  // NOT OK, protected

  sub_derived.g();  // OK
  sub_derived.h();  // NOT OK, Compilation Error
  
  return 0;
}
```
**Примечание 1:** по умолчанию классы наследуются приватно, а структуры -- публично. Второе главное отличие.

**Примечание 2:** все модификаторы доступа определяются на уровне компиляции. Если на уровне теста доступности поля/метода видно, что доступа к ним нет, то выдается ошибка. Программа не компилируется

### Видимость и доступность

Иногда возможно написание следующего кода. В этом случае локальные функции и поля "затеняются" более локальными версиями этих функций.

```cpp=
struct Mom {
    int a = 0;
    void f() {
        std::cout << "Mom\n";
    }
};

struct Son : Mom {
    int a = 1;
    void f() {
        std::cout << "Son\n";
    }
};

struct Son2: Mom {
private:
    int a = 1;
    void f() { // not accesible but visible within context
        std::cout << "Son2\n";
    }
}

int main() {
    Mom s;
    s.f(); // print "Son\n"
    s.Mom::f(); // print "Mom\n"
    std::cout << s.a << std::endl; // print 1
    std::cout << s.Mom::a << std::endl; // print 0
}
```

## Порядок инициализации полей

Перед выполнением конструктора следует инициализация полей класса. Если одно из полей класса является структурой, или же классом, то при инициализации полей основного класса следует первоначальная инициализация структуры/класса-поля. 

Пример:
```cpp=
struct Atom {
  int Z;
  int A;
  Atom() : Z(0), A(1) { }
};

class Material {
  protected:
    Atom base;
    int n_atoms;
  
  public:
    Material() : base(Atom()), n_atoms(0) { }
};

class DerivedMaterial: public Material {
  private:
    double temperature;
  public:
    DerivedMaterial() : Material(), temperature(273.15) { }
};
```

## Порядок конструкторов/деструкторов при наследовании

При создании класса без наследования сначала создаются и инициализируются поля, затем вызывается сам конструктор класса. Это правило сохраняется и при наследовании: сначала создается родительская часть со своими полями, затем инициализируются поля дочернего класса, затем конструктор дочернего класса. Деструкторы соответственно вызываются в обратном порядке.

```cpp=
struct A {
    A() {
        std::cout << "A\n";
    }
    ~A() {
        std::cout << "~A\n";
    }
};

struct B {
    B() {
        std::cout << "B\n";
    }
    ~B() {
        std::cout << "~B\n";
    }
};

struct Mom {
    A a;
    Mom() {
        std::cout << "Mom\n";
    }

    ~Mom() {
        std::cout << "~Mom\n";
    }

    Mom(int x) : x(x) {}

private:
    int x = 0;
};

struct Son: Mom {
    B b;
    Son() {
        std::cout << "Son\n";
    }
    ~Son() {
        std::cout << "~Son\n";
    }
    Son(int x) : Mom(x), x(x) {}

    // since c++11
    // allow to use base class constructors
    using Mom::Mom;
private:
    int x = 0;
};

int main() {
    Son s; // print AMomBSon~Son~B~Mom~A
}
```
## Приведение типов при наследовании

На практике часто может потребоваться писать много функций, которые могут принимать либо базовый, либо класс наследника. В случае если класс является публичным наследником родителя, то можно приводить тип наследника к родителю и использовать его поля/методы.

```cpp=
struct Base {
    int a = 0;
};

struct Derived: public Base {
    int a = 1;
};

void f(Base& b) {
    std::cout << b.a << '\n';
}

void ff(Base* b) {
    std::cout << b->a << '\n';
}

void fff(Base b) {
    std::cout << b.a << '\n';
}

int main() {
    Derived b;
    // implicit cast to base class reference
    f(d); // OK // print 0
    // implicit cast to base class pointer
    ff(&d); // OK // print 0
    // creates copy of base class
    fff(d);  // OK
}
```

При наследовании можно приводить объекты дочерних классов к родительским, но не наоборот.

```cpp=
struct Base {
    int a = 0;
    Base() = default;
    Base(const Base& b) {
        std::cout << "A";
    }
};

struct Derived: public Base {
    int a = 1;
    Derived() = default;
};

void f(Base b) {
    std::cout << b.a << '\n';
}

int main() {
    // cast from heir to base
    Derived d;
    Base& b = d;
    
    Derived& dd = b; // CE, because Derived class heirs privately from Base, so outside we don't know
                     // that Derived is heir of Base
    
    
    // dark magic
    Derived& dd = static_cast<Derived&>(&b); // OK
}
```

### Немного темной магии

Так как в С++ объекты не могут быть размера 0, то размер пустых классов и структур равен 1. Однако при этом при наследовании это правило игнорируется и размер пустого класса не учитывается.

```cpp=
struct GrandBase {

};

struct Base: public GrandBase {
    int a = 0;
    Base() = default;
    Base(const Base& b) {
        std::cout << "A";
    }
};

struct Derived: public Base {
    int a = 1;
    Derived() = default;
};

void f(Base b) {
    std::cout << b.a << '\n';
}

// Empty base optimization
// [  Base  ][  Derived  ]

int main() {
    // cast from heir to base
    Derived d;
    Base& b = d;
    GrandBase gb;
    std::cout << sizeof(gb) << '\n'; // print 1
    std::cout << sizeof(b) << '\n'; // print 4
    std::cout << b.a << '\n'; // 0
    std::cout << d.a << '\n'; // 1
    std::cout << *(&b.a + 1) << '\n'; // 1

    // cast from base to heir
    //Derived dd = b; // CE
    //Derived& dd = b; // CE
    //Derived* dd = &b; // CE
    
    // если очень хочется скастовать вниз по иерархии
    Derived& dd = static_cast<Derived&>(&b);

}
```

# Множественное наследование

В С++ возможно множественное наследование. Это позволяет создать новый класс на основе нескольких других других (расширить их).

Синтаксис множественного наследования:
```cpp=
class MyClass: public A, private B, protected C {
    
}; 
```

При множественном наследовании порядок объявления наследования определяет порядок вызова конструкторов родительских классов и их расположение в памяти.

```cpp=
struct Mom {
    void f() { std::cout << "Mom::f" << '\n'; }
    int m = 1;
};

struct Dad {
    void f() { std::cout << "Dad::f" << '\n'; }
    int d = 2;
};

struct Son: public Mom, public Dad { // от порядка записи зависит порядок хранения
    int s = 3;
};

// [ Mom::m ][ Dad::d ][ Son::s ]

int main() {
    Son s;
    s.f(); // создает неоднозначность какую именно f вызвать,
    // если бы у сына был свой f, то вызвался бы он
    s.Mom::f(); // OK // обращение к родительскому методу напрямую
    std::cout << sizeof(s) << '\n'; // prints 12
    std::cout << &s.m << ' ' << &s.d << &s.s << '\n'; // difference 4 bytes
}
```

## Порядок конструкторов и деструкторов при множественном наследовании

Порядок инициализации полей и вызова конструкторов/деструкторов соответствует такому же как при обычном наследовании с учетом порядка множественного наследования .

```cpp=
class A {
public:
    A() {
        std::cout << "A()\n";
    }
    ~A() {
        std::cout << "~A()\n";
    }
};

class B {
public:
    B() {
        std::cout << "B()\n";
    }
    ~B() {
        std::cout << "~B()\n";
    }
};

class Base {
    A a;
public:
    Base() {
        std::cout << "Base()\n";
    }
    ~Base() {
      std::cout << "~Base()\n";
    }
};
 
class Base2 {
    B b;
public:
    Base2() {
        std::cout << "Base2()\n";
    }
    ~Base2() {
      std::cout << "~Base2()\n";
    }
};
 
class Derived: public Base, public Base2 {
public:
    Derived(): Base(), Base2() {
        std::cout << "Derived()\n";
    }
    ~Derived() {
      std::cout << "~Derived()\n";
    }
};

int main() {
  Derived d; // A()Base()B()Base2()Derived()~Derived()~Base2()~B()~Base()~A()
  return 0;
}
```

## Приведение типов при множественном наследовании

Приведение типов при множественном наследовании такое же как при одиночном. Т.е. легко привести от наследника к любому из родителей, но невозможно в обратную сторону без темной магии.

```cpp=
class Base {
public:
    Base() {
        std::cout << "Base()\n";
    }
    ~Base() {
      std::cout << "~Base()\n";
    }
};
 
class Base2 {
public:
    Base2() {
        std::cout << "Base2()\n";
    }
    ~Base2() {
      std::cout << "~Base2()\n";
    }
};
 
class Derived: public Base, public Base2 {
public:
    Derived(): Base(), Base2() {
        std::cout << "Derived()\n";
    }
    ~Derived() {
      std::cout << "~Derived()\n";
    }
};

int main() {
    Derived d;
    Base& b = d; // OK
    Base2& b2 = d; // OK
    Derived* dd = &b; // CE
    return 0;
}
```

Кроме того, возможно привести один тип родителя к другому, но только через приведение типа к дочернему.

```cpp=
class A {
    
};

class B {
    
};

class C : public A, public B {
    
};

int main() {
    A *pa = new C();
    B *pb = static_cast<B*>(pa); // CE
    B *pb = static_cast<B*>(static_cast<C*>(pa));
}
```
Еще один пример работы оператора приведения типов:

```cpp=
struct Granny {
    int g = 0;
};

struct Mom: public Granny {
    int m = 1;
};

struct Dad: public Granny {
    int d = 2;
};

struct Son: public Mom, public Dad {
    int s = 3;
};

// [ Mom::g ][ Mom::m ][ Dad::g ][ Dad::d ][ Son::s ]
int main() {
    Son s;
    Dad* d = &s; // OK, но имеет под собой магию сдвига указателя с начала сына на начало папы
    std::cout << &s << ' ' << d << '\n'; // разница указателей 8 байт

    Son* ss = static_cast<Son*>(pd); // OK
    Granny& g = s; // CE, ambigious cast
    Granny* g = static_cast<Granny*>(&s); // CE, ambigious cast
}
```

## Проблема ромба

При множественном наследовании создается две копии прародительского класса для классов родителей, это приводит к тому, что компилятор не может выбрать автоматически к какому полю и методу пытается обратиться пользователь. Кроме того, по сути в дочернем классе хранится лишняя копия прародителя.

```cpp=
struct Granny {
    int g = 0;
};

struct Mom: public Granny {
    int m = 1;
};

struct Dad: public Granny {
    int d = 2;
};

struct Son: public Mom, public Dad {
    int s = 3;
};

// [ Mom::g ][ Mom::m ][ Dad::g ][ Dad::d ][ Son::s ]
// Diamond problem
int main() {
    Son s;
    Dad* d = &s; // OK, но имеет под собой магию сдвига указателя с начала сына на начало папы
    std::cout << &s << ' ' << d << '\n'; // разница указателей 8 байт

    Son* ss = static_cast<Son*>(pd); // OK
    Granny& g = s; // CE, ambigious cast
    Granny* g = static_cast<Granny*>(&s); // CE, ambigious cast
}
```

### Inaccessible base class

```cpp=
class Granny {
public:
  int g;  
};

class Mom : public Granny {
public:
  int m;  
};

// can't access Granny part of Son
class Son : public Mom, public Granny {
public:
  int s;  
};

int main() {
    // in memory
    // [ Granny::g ][ Mom::m ][ Granny::g ][ Son::s ]
    Son s;
    s.g; // CE (ambiguous call)
    s.Mom::g; // OK
    s.Granny::g; // CE (ambiguous call)
}

```

## Виртуальное наследование

Виртуальное наследование позволяет избежать проблемы ромбовидного наследование. Специальный механизм, который позволяет не создавать копию уже отнаследованного класса.

```cpp=
struct Granny {
    int g = 0;
};

struct Mom : public virtual Granny {
    int m = 1;
};

struct Dad : public virtual Granny {
    int d = 2;
};

struct Son: public Mom, public Dad { // от порядка записи зависит порядок хранения
    int s = 3;
};

// [ mom_ptr ][ Mom::m ]...[ dad_ptr ][ Dad::d ]...[ Son::s ][ Granny:g ]

int main() {
    Son s;
    Dad* pd = &s;

    pd->g;

    std::cout << &s.Dad::d << ' ' << &s.Mom::m << ' ' << &s.Son::s << '\n';
    std::cout << &s.Dad::g << ' ' << &s.Mom::g << '\n';
}
```
Ошибок при попытке достучаться до `g` нет. Вопрос: почему бы все не делать `virtual`?

:::spoiler Подсказка:
Посмотрите на `sizeof(s)`. Он увеличился в два раза!!!
::: 

Формальное расположение объектов в памяти описывается следующей диаграммой:
```cpp=
// [ mom_ptr ][ Mom::m ]....[ dad_ptr ][ Son::s ][ Granny::g ]
```

При попытке преобразовать объект `Son` в `Dad` или `Mom` нам потребуется доступ к полю `Granny::g`. Попытка достучатся к этому полю будет происходить через "указатель" в конце нашего объекта (в конце относительно понятия памяти).
Виртуальное наследование накладная штука...

Виртуальное наследование может быть частичным. Например, `Mom` наследует `Granny` виртуально, а `Dad` нет. Тогда механизм одной `Granny` не сработает (в памяти будет находится виртуальная `Granny` от `Mom` и явная  от `Dad`). Условно, количество копий повторяющегося класса можно посчитать как количество явных объявлений + (1 если есть хоть одно виртуальное объявление, иначе 0)

# Виртуальные функции

## Полиморфизм и виртуальные функции

Желание объединить методы, которые могут по разному работать с объектами, но по своей сути давать ответ на один и тот же вопрос, породило полиморфизм.

Виртуальной функцией назовем такую функцию, которая будет преимущественно вызываться от частного типа, а не от общего. При получении на вход объекта-наследника будем вызвать именного его методы, а не методы класса-родителя.

```cpp=
// полиморфизм - принцип, согласно которому для разных типов одна команда выполняет разное действие

struct Base {
    void f() { std::cout << "Base\n"; }
};

struct Derived : Base {
    void f() { std::cout << "Derived\n"; }
};

struct VBase {
    virtual void f() { std::cout << "Base\n"; }
};

struct VDerived : VBase {
    void f() { std::cout << "Derived\n"; } // наследники тоже имеют виртуальную функцию
};

// виртуальная функция - это такая функция, для которой не важно обращение идет к общему или частному типу

int main() {
    Derived d;
    Base& b = d;
    d.f();
    b.f();
    VDerived vd;
    VBase& vb = vd;
    vd.f();
    vb.f();
}
```
Пример выше демонстрирует следующий факт: если метод (функция внутри класса) не является виртуальной, то при обращении к объекту по ссылке родителя мы получаем метод (функцию) из родительского класса.
Если же метод был объявлен виртуальным, то при обращении ссылкой родителя на производный объект будет вызываться метод производного объекта. 
Устанавливается универсальный способ обращения к производным объектам по ссылке родителя и при этом методы вызываются переопределенные для соответствующих производных классов.

Пример виртуальной функции и функции подобной сигнатуры с другим типом аргумента:
```cpp=
struct VBase {
    virtual void f(int) { std::cout << "Base virtual\n"; }
    void f(double) { std::cout << "Base NON-virtual\n"; }
};

struct VDerived : VBase {
    void f(double) { std::cout << "Derived NON-virtual\n"; }
};

int main() {
    VDerived vd;
    VBase& vb = vd;
    vd.f(0);
    vb.f(0);
    return 0;
}
```
Компилятор видит, что экземпляру класса `VDerived` на вход попал `int`. Никакой функции `f`, которая принимала бы `int` нету, а значит мыковертируем `int` и используем переопределенный метод класса `VDeriived`. Во втором случае мы обращаемся с объектом как с базовым. Тогда и метод берется из базового класса.

Пример с приватной виртуальной функцией
```cpp=
#include <iostream>

struct Base {
    virtual void f(int) const { std::cout << "Base\n"; }
};

struct Derived: Base {
private:
    virtual void f(int) const override { std::cout << "Derived\n"; }
};

int main() {
    Derived d;
    Base& b = d;
    b.f(0); // OK Derived
}
```
На уровне компилятора решается вопрос о приватности/публичности/доступности/видимости методов/полей. Представленный выше код скомпилируется с пониманием того, что у базового класса есть виртуальный метод, но вот уже при вызове такого метода мы вызовем самый локальный метод для ссылки на экземпляр производного класса.

## Override

Допустим произошла незначительная ошибка в переопредлении функции базового класса. Мы забыли установить `const`? Будет ли тогда наше объявление в классе-наследнике переопредять виртуальну функцию. Ответ -- нет. 
Для того чтобы компилятор на этапе компиляции подсказал, что мы не смогли переопределить виртуальную функцию базового класса используется ключевое слово `override`.

```cpp=
#include <iostream>

struct Base {
    virtual void f(int) const { std::cout << "Base\n"; }
};

struct Derived : Base {
    int a = 0;
    // since c++11
    virtual void f(int) override { std::cout << "Derived\n"; } // CE, нет const
};

struct GrandDerived : Derived {
    virtual void f(int) const override final { std::cout << "GrandFinal\n"; } // final чтобы нельзя было в дальнейшем переопределить
};

int main() {
    Derived d;
    Base& b = d;
    d.f(0);
    b.f(0);
}

```
В коде выше класс `Derived` неправильно переопределил метод `void f(int) const`. Компилятор это отследил благодаря ключевому слову `override`.
Тут же опрежелен класс наследник `GrandDerived` и внутри него метод `void f(int) const` переопределяется с указанием `override final`. `final` указывает на то, что это будет последняя перегрузка данного метода. При последующем наследовании мы уже не сможем перегрузить соответствующий метод. `final` можно применять и только к виртуальным методам (функциям).

## Абстрактный класс и чисто виртуальная функция

Ряд классов может быть описан общим классом-родителем. Такой класс-родитель называют интерфейсом.
Интерфейс содержит в себе лишь объявления методов, но не их реализацию. Чтобы каждый метод работал в соответствующем экземпляре класса как нужно, он должен быть виртуальным.
Интерфейс в себе не несет реализацию виртуальных методов. Соответственно, они являются чисто виртуальными (pure virtual).
Если класс содержит хоть один **чисто виртуальный метод**, то он называется абстрактным. Если класс-наследник не переопредили чисто виртуальный метод, то и он будет абстрактным.

```cpp=
#include <iostream>

// Abstract class - нельзя создать объект от такого класса
struct Base {
    // pure virtual
    virtual void f() = 0;
};

struct Derived: Base {
    virtual void f() override { std::cout << "Derived\n"; }
};

int main() {
    Base b; // CE
    Derived d;
    d.f();
    Base& b = d;
    b.f();
}
```
Создать экземпляр абстрактного класса нельзя, но можно создать ссылку на такой объект.



## Примеры с виртуальными и не виртуальными функциями

Пример 1:
```cpp=
#include <iostream>

struct Base {
    virtual void f(int) { std::cout << "Base\n"; }
    void f(double) { std::cout << "Non virtual Base\n"; }
};

struct Derived : Base {
    int a = 0;
    void f(double) { std::cout << "Derived\n"; }
};

int main() {
    Derived d;
    Base& b = d;
    d.f(0.0); // Derived
    b.f(0.0); // Non virtual Base
}

```
Пример 2:    
```cpp=
#include <iostream>

struct Base {
    virtual void f(int) { std::cout << "Base\n"; }
    void f(double) { std::cout << "Non virtual Base\n"; }
};

struct Derived : Base {
    int a = 0;
    void f(double) { std::cout << "Derived\n"; }
};

struct GrandChild: Derived {

};

struct Mom {
    virtual void f() { std::cout << "Mom\n"; }
};

struct Dad {
    void f() { std::cout << "Dad\n"; }
};

struct Son: Mom, Dad {
    void f() { std::cout << "Son\n"; }
};

int main() {
    GrandChild c;
    Base& b = c;

    b.f(0.0); // Non virtual Base

    Son s;
    s.f(); // Son
    Mom& m = s;
    m.f(); // Son
    Dad& d = s;
    d.f(); // Dad
}
```

## Виртуальный деструктор

Абстрактный класс может в себе содержать поля и инициализировать их по умолчанию. Например, пусть абстрактный класс содержит указатель на динамически выделяемый `int`. В деструкторе класса описывается логика освободения памяти данного поля.
От абстрактного класса наследуется `Derived` со своим динамическим полем и реализован свой собственный деструктор. При этом при создании объекта его тип приводится к родительскому, что в итоге приводит к утечке памяти.

Рассмотрим следующий пример:
```cpp=
struct Base {
    int* pb = new int();
    ~Base() { // add virtual to resolve problem
        std::cout << "~Base\n";
        delete pb;
    }
};

struct Derived: Base {
    int* db = new int();
    ~Derived() {
        std::cout << "~Derived\n";
        delete pb;
    }
};

int main() {
    Base* b = new Derived(); // pb deleted, dp not deleted
    delete b; // ~Base without virtual destructor
}
```
Деструктор класса `~Base()` вызовется, а вот деструктор `~Derived()` нет. В таком случае надо объявить деструктор класса `Base` как виртуальный.
Делайте деструктор виртальным. Это позволит избежать утечки памяти.

## Как это устроено

### Статический и динамический типы

Полиморфный объект -- есть хотя бы одна **виртуальная функция**.
В следующем куске кода показано, что до момента запуска компилятор не может определить какой именно класс придет на вход переменной `bb`. Соответственно, нужно понять какой именно метод требуется вызвать (RunTimeTypeInformation, [typeid](https://en.cppreference.com/w/cpp/language/typeid)). 
```cpp=
include <iostream>

struct Base {
  virtual void f() {
    std::cout << "Base\n";
  }
}

struct Derived: public Base {
  void f() override {
    std::cout << "Derived\n";
  }
}

int main() {
  int n;
  std::cin >> n;

  Derived d;
  Base b;
  
  Base& bb = (n % 2 ? b : d);
  
  bb.f();
  std::cout << typeid(bb) << "\n";
  return 0;
}
```
Класс `Base` является в таком случае статическим -- обращение к его виртуальному методу является однозначным и не требует разрешение каких-либо конфликтов. 

### dynamic_cast

Данный тип каста позволяет *переходить* с одной ветки наследования на другую. В случае ошибки *выкидывается* исключение, которое в дальнейшем можно обработать
```cpp=
#include <iostream>

struct Granny {
  virtual void foo() { std::cout << "Granny\n"; }
};

struct Mom: public Granny {
  void foo() { std::cout << "Mom\n"; }
};

struct Dad: public Granny {
  void foo() { std::cout << "Dad\n"; }
};

struct Son: public Mom, public Dad {
  void foo() { std::cout << "Son\n"; }
};

int main() {
  Son s;
  
  s.foo();  // OK -> Son

  Mom* pm = &s;
  Mom& m = s;

  Mom mom;
  
  Dad& f1 = dynamic_cast<Dad&>(s);  // OK
  pf1->foo();
  Mom* pm1 = dynamic_cast<Mom*>(pf1);  // OK
  pm1->foo();

  Dad& f1 = dynamic_cast<Dad&>(s);  // OK
  f1.foo();
  Dad& f2 = dynamic_cast<Dad&>(mom);  // not OK

  return 0;
}

```

### vtable

Каждый объект с определенной виртуально функцией содержит в себе указатель на область статической памяти. Указатель приводит к *vtable*. 
vtable -- область памяти хранящая информацию о `typeinfo` и адреса на виртуальные функции, которые реализованы в классе.

**Примечание 1:** Виртуальная таблица создается **один** раз для класса, но хранится в каждом объекте этого класса

Устроено это следующим образом: компилятор видит, что от полиморфного объекта требуется вызвать функцию. Если функция является виртуальной, то по указателю на vtable компилятор идет за определением данной функции.
В таблице виртуальные функции находятся в порядке определения внутри класса.

Виртуальная таблица для простого класса:
```cpp=
struct A {}; // sizeof(A) = 1

struct B {
    virtual void f() {}; // sizeof(B) = 8
};

// B_vtable
// [[ typeinfo_ptr ][ &B::f ]]
```

Виртуальная таблица при наследовании:
```cpp=
struct A {}; // sizeof(A) = 1

struct Base {
    int b;
    virtual void f() {}; // sizeof(Base) = 16
};

struct Derived: public Base { // sizeof(Derived) = 16
    int d;
    void f() override {};
    virtual void g() {};
};
// Derived memory layout
// [[ derived_vptr ][ b ][ d ]]
// Derived vtable
// [[ typeinfo_ptr ][ &Derived::f ][ &Derived::g ]]

int main() {
    Base b;
    b.f(); // goes to B_table and call f
    
    Derived d;
    Base& bb = d;
    bb.f(); // goes to Derived object vtable and gets it's version
}
```

**Примечание 1**: Виртуальная таблица создается для каждого типа, а не объекта. Объекты этих типов только хранят её.

#### Виртуальные функции при множественном наследовании

```cpp=
struct Granny {
    int g;
    virtual void fg() {};
};
struct Mom: public Granny {
    int m;
    virtual void fm() {};
};
struct Dad: public Granny {
    int d;
    virtual void fd() {};
};
struct Son: public Mom, public Dad {
    int s;
    virtual void fs() {};
};
```

:::spoiler Сколько будет указателей на виртуальные таблицы?
2, так как при только одном объект типа Son будет выглядеть следующим образом и возможности привести объект типа Son нельзя будет привести к типу Dad.
```cpp=
[[ son_vptr ][[ g ][ m ]][[ g ][ d ]][ s ]]

int main {
    Son s;
    Dad& d = s;
    s.fd();
}
```

Поэтому в реальности выглядит это так:
```cpp=
[[ vptr_1 ][[ g ][ m ]][[ vptr_2 ][ g ][ d ]][ s ]]
```

Причем указатели vptr_1 и vptr_2 указывают на разные виртуальные таблицы, так как иначе типы Dad и Mom были бы неразличимы.
:::

#### Виртуальные функции при виртуальном наследовании

```cpp=
struct Granny {
    int g;
    virtual void f() {};
};
struct Mom: virtual public Granny {
    int m;
};
struct Dad: virtual public Granny {
    int d;
};
struct Son: public Mom, public Dad {
    int s;
};

// Son's memory layout
// [ mom_vptr ][ m ]...[ dad_vptr ][ d ][ s ][ granny_vptr ][ g ]

// Mom-in-son vtable
// [ virtual_offset ][ top_offset ][ typeinfo_ptr ][ &f ]
// virtual_offset - сдвиг до предков
// top_offset     - сдвиг до начала объекта

```

## Всякие интересные примеры с виртуальностью

Пример 1:
```cpp=
struct Base {
    Base() {
        f(0);
    }
    virtual void f(int x) const {
        std::cout << "Base " << x << '\n'; 
    }
};

struct Derived: public Base {
    Derived() {
        f(1);
    }
    virtual void f(int x) const {
        std::cout << "Derived " << x << '\n'; 
    }
};

int main() {
    const Base& b = Derived();
    b.f(2);
    // Base 0
    // Derived 1
    // Derived 2
}
```

Пример 2:

Линковщик не может вызвать функцию f из конструктора Base
```cpp=
struct Base {
    Base() {
        f(0);
    }
    virtual void f(int x) const = 0;
};

struct Derived: public Base {
    Derived() {
        f(1);
    }
    void f(int x) const override {
        std::cout << "Derived " << x << '\n'; 
    }
};

int main() {
    const Base& b = Derived(); // Linker error
    b.f(2);
}
```

Пример 3:

Линковщик не может создать виртуальную таблицу для класса.
```cpp=
struct Base {
    virtual void f();     
};

int main() {
    Base b;
}
```


