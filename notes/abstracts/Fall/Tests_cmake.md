---
title: Tests and CMake
tags: cpp, abstract, 2sem
---

# CMake

## CMake build

# Test framework

## V1
```cpp=
#include <iostream>
#include <cassert>

int good_sum(int x, int y) {
    return x + y;
}

int bad_sum(int x, int y) {
    return x + y - 1;
}

void TestSum1() {
    assert(good_sum( 2,  3) ==  5);
    assert(good_sum(-2, -3) == -5);
    assert(good_sum(-2,  2) ==  0);
    std::cout << "TestSum OK" << std::endl;
}

void TestSum2() {
    assert(bad_sum( 2,  3) ==  5);
    assert(bad_sum(-2, -3) == -5);
    assert(bad_sum(-2,  2) ==  0);
    std::cout << "TestSum OK" << std::endl;
}
```

### Плюсы
* Как-то проверяет написанный код

### Минусы
* Легко обойти тесты
* Тяжело писать свои тесты
* На первом же упавшем тесте программа завершается 

## V2

```cpp=
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <cassert>

/*
*  
*  Two words are synonyms if there is a mapped word for them
*
*/


using Synonyms = std::map<std::string, std::set<std::string>>;

// сократили запись типа и везде изменили на Synonyms
void AddSynonyms(Synonyms &synonyms, const std::string &first_word, const std::string &second_word) {
    synonyms[second_word].insert(first_word);
    synonyms[first_word].insert(first_word); // тут должен не сработать AddSynonyms
}

size_t GetSynonymCount(Synonyms &synonyms, const std::string &word) {
    return synonyms[word].size();
}

bool AreSynonyms(Synonyms &synonyms, const std::string &first_word, const std::string &second_word) {
    return synonyms[first_word].count(second_word) == 1;
}

void TestAddSynonyms() { // тестируем AddSynonyms
    {
        Synonyms empty; // тест 1
        AddSynonyms(empty, "a", "b");
        const Synonyms expected = {
            {"a", {"b"}}, // ожидаем, что при добавлении синонимов появятся две записи в
            // словаре
            {"b", {"a"}}};
        assert(empty == expected);
    }
    {
        // заметим, что мы формируем корректный словарь и ожидаем, что он останется корректным
        Synonyms synonyms = {              // если вдруг корректность нарушится, то assert скажет, где
                             {"a", {"b"}}, // тест 2
                             {"b", {"a", "c"}},
                             {"c", {"b"}}};
        AddSynonyms(synonyms, "a", "c");
        const Synonyms expected = {
            {"a", {"b", "c"}},
            {"b", {"a", "c"}},
            {"c", {"a", "b"}}};
        assert(synonyms == expected);
    }
    std::cout << " TestAddSynonyms OK" << std::endl;
}

void TestCount() { // тестируем Count
    {
        Synonyms empty;
        assert(GetSynonymCount(empty, "a") == 0);
    }
    {
        Synonyms synonyms = {
            {"a", {"b", "c"}},
            {"b", {"a"}},
            {"c", {"a"}}};
        assert(GetSynonymCount(synonyms, "a") == 2);
        assert(GetSynonymCount(synonyms, "b") == 1);
        assert(GetSynonymCount(synonyms, "z") == 0);
    }
    std::cout << " TestCount OK" << std::endl;
}

void TestAreSynonyms() { // тестируем AreSynonyms
    {
        Synonyms empty; // пустой словарь для любых двух слов вернёт false
        assert(!AreSynonyms(empty, "a", "b"));
        assert(!AreSynonyms(empty, "b", "a"));
    }
    {
        Synonyms synonyms = {
            {"a", {"b", "c"}},
            {"b", {"a"}},
            {"c", {"a"}}};
        assert(AreSynonyms(synonyms, "a", "b"));
        assert(AreSynonyms(synonyms, "b", "a"));
        assert(AreSynonyms(synonyms, "a", "c"));
        assert(AreSynonyms(synonyms, "c", "a"));
        assert(!AreSynonyms(synonyms, "b", "c"));
        assert(!AreSynonyms(synonyms, "c", "b"));
        // false
        // false
    }
    std::cout << " TestAreSynonyms OK" << std::endl;
}

void TestAll() { // функция, вызывающая все тесты
    TestCount();
    TestAreSynonyms();
    TestAddSynonyms();
}
```

### Плюсы
* Как-то проверяет написанный код

### Минусы
* При проверке равенства в консоль не выводятся значения сравниваемых переменных. И нет возможности узнать чему была равна переменная
* После невыполненого `assert` код падает. Если в `TestAll` поставить `TestAddSynonyms` на первое место, то остальные два теста даже не начнутся
* Результаты тестов выводят OK в стандартный вывод и смешиваются с тем, что должен выводить сам исполняемый код

## V3

Исправим недочет V2 и добавим следующий код:
```cpp=
template<class T, class U>
void AssertEqual(class T& t, class U& u) {
    if (t != u) {
        std::ostringstream os;
        os << "Assertion failed: " << t << "!=" << u;
        throw std::runtime_error(os);
    }
}
```

Тогда вызов тестов будет выглядеть следующим образом:

```cpp=
void TestCount() { // тестируем Count
    {
        Synonyms empty;
        AssertEqual(GetSynonymCount(empty, "a"), 0); // comparison of signed and unsigned...
        AssertEqual(GetSynonymCount(empty, "b"), 0);
    }
```

Для работоспособности необходимо исправить сравнение типов, а также перегрузить операторы вывода для наших типов, т.к. хотелось бы видеть что с чем сравнивается. Кроме того, в этом виде нет информации о том, какой именно тест не прошел, поэтому добавим строчку информации об этом.

```cpp=
template <class T, class U>
void AssertEqual(class T &t, class U &u, const std::string &hint) {
    if (t != u)
    {
        std::ostringstream os;
        os << "Assertion failed: " << t << "!=" << u << " Hint: " << hint << "\n";
        throw std::runtime_error(os);
    }
}

void TestCount() { // тестируем Count
    {
        Synonyms empty;
        AssertEqual(GetSynonymCount(empty, "a"), 0, " Synomym count for empty dict a");
        AssertEqual(GetSynonymCount(empty, "b"), 0, " Synomym count for empty dict b");
    }
    {
        Synonyms synonyms = {
            {"a", {"b", "c"}},
            {"b", {"a"}},
            {"c", {"a"}}};
        AssertEqual(GetSynonymCount(synonyms, "a"), 2u, " Nonempty dict , count a");
        AssertEqual(GetSynonymCount(synonyms, "b"), 1u, " Nonempty dict , count b");
        AssertEqual(GetSynonymCount(synonyms, "z"), 0u, " Nonempty dict , count z");
    }
    std::cout << " TestCount OK" << std::endl;
}
```

Например такая перегрузка операторов вывода.

```cpp=
template <class T>
std::ostream &operator<<(std::ostream &os, const std::set<T> &s)
{
    os << "{";
    bool first = true;
    for (const auto &x : s)
    {
        if (!first)
        {
            os << ", ";
        }
        first = false;
        os << x;
    }
    return os << "}";
}

template <class K, class V>
std::ostream &operator<<(std::ostream &os, const std::map<K, V> &m)
{
    os << "{";
    bool first = true;
    for (const auto &kv : m)
    {
        if (!first)
        {
            os << ", ";
        }
        first = false;
        os << kv.first << ": " << kv.second;
    }
    return os << "}";
}
```

Теперь в силу выброса исключений придется оборачивать каждый вызов теста в блок `try-catch`. Чтобы это избежать сделаем еще одну обертку:

```cpp=
template <class TestFunc>
void RunTest(TestFunc func)
{
    try
    {
        func();
    }
    catch (std::runtime_error &e)
    {
        std::cerr << test_name << " fail: " << e.what() << std::endl;
    }
}
```

### Плюсы

* Теперь можно практически автоматически запускать тесты и собирать информацию о провалившихся тестах
* Провалившиеся тесты не завершают программу
* Стандартный поток и поток ошибок не смешиваются

### Минусы

* Не хватает менеджера тестов

## V4

Добавим менеджер тестов:
```cpp=
class TestRunner
{
public:
    template <class TestFunc>
    void RunTest(TestFunc func, const std::string &test_name)
    {
        try
        {
            func();
            cerr << test_name << " OK" << endl;
        }
        catch (runtime_error &e)
        {
            ++fail_count;
            cerr << test_name << " fail: " << e.what() << endl;
        }
    }

    ~TestRunner()
    {
        if (fail_count > 0)
        {
            std::cerr << fail_count << " unit tests failed. Terminate" << std::endl;
            exit(1);
        }
    }

private:
    int fail_count = 0;
};
```

# Сборка с помощью CMake

Для сборки проектов из множества файлов используют утилиту [CMake](https://cmake.org/) ([документация](https://cmake.org/cmake/help/latest/index.html), [туториал](https://cmake.org/cmake/help/latest/guide/tutorial/index.html)).
Сборка с помощью утилиты cmake требует предварительной установики самой утилиты. Интересный гайд для windows можно найти здесь: [вот здесь](https://www.youtube.com/watch?v=HRGZDnNMc5U&feature=emb_logo).
Для работы из под [MSys2](https://www.msys2.org/), [MSys2 cmake usage](https://www.msys2.org/docs/cmake/).
Для тестового примера работы с CMake перейдем к [проекту](https://gitlab.com/agordov/cpp).

- cmake_minimum_required -- указывает минимальную версию cmake, которая нужна для сборки проекта;
- set -- установка значения переменной; первый агрумент -- переменная, последующие -- значение;
- add_compiler_options -- одбавление флагов компиляции;
- add_executable -- указывает на финальный исполняемый файл; первый аргумент -- название исполняемого файла, последующие --исполняемые файлы
- include_directories -- указывает на директории, которые требуется включить для получения описания классов и прочего
- Переменные состоят из заглавных букв. Подстановка значения переменной происходит с помощью следующего синтаксиса
```cmake
${MY_VARIABLE}
```
- Есть возможность писать ветвления с помощью директивы `if-elseif-endif`

В заголовочных файлах описывают класс и какие библиотеки нужны для работы ряда методов класса. Если класс не один, то возможно повторение подключаемых библиотек.
Например, подключение своих библиотек два раза приводит к ошибке. Ошибка возникает из-за двойной вставки описания класса. Избежать этого можно с помощью двух методов:
- директива `#ifndef MY_CLASS_H\n#define MY_CLASS_H`
- директива `#pragma once`
укажание данных директив в начале файле позволяет избежать повторной вставки описания класса при компиляции проекта.
На эту тему можно посмотреть вопрос на [stackoverflow](https://ru.stackoverflow.com/questions/630573/ifndef-define-vs-pragma-once). Суть одна -- директива `#pragma once` может не поддерживаться компилятором, но такое случается редко.

# Задание

Ниже расположен код проекта, реализующего класс рациональных чисел `Rational` и класс комплексных чисел `Complex`.
Вам требуется разделить код на следующие компоненты:
```=
project/
├── CMakeLists.txt
├── include/
│   ├── Rational.hpp
│   └── Complex.hpp
│   └── src/
│       ├── Complex.cpp
│       └── Rational.cpp
└── tests/
    ├── test_complex.cpp
    └── test_rational.cpp 
```

Требуется написать тесты, которые бы покрыли основную логику классов:
- Дроби:
  - Проверка конструктора по умолчанию (проходит инициализация предсказываемых числителя и знаменателя)
  - Проверка неупрощаемых дробей
  - Проверка дробей с нулевым числителем
  - Проверка отрицательного знаменателя
  - Проверка упрощения
- Комплексные числа:
  - Проверка корректности конструктора
  - Проверка арифметических операций с double и Complex
  - Проверка взятия действительной и мнимой частей
  - Проверка оператора равенства

[Код для работы:](https://hackmd.io/@zachet-cpp-mipt-2022/HyOuTSr8o)

Примерный план CmakeLists.txt:
```cmake=
cmake_minimum_required(VERSION 3.10)

project(rational_framework)
set(CMAKE_CXX_STANDARD 17)

set(PROGRAM_SRCS
    main.cpp
    include/src/Rational.cpp
    include/src/Complex.cpp
)

include_directories(
    include/
)

add_executable(rational_framework ${PROGRAM_SRCS})
```