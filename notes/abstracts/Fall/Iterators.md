---
title: iterators
tags: cpp, abstracts, 2sem
---

# Кто такие итераторы (идея)

Любой стандартный контейнер из STL представляет собой последовательность. У последовательностей есть начало и конец. Пользователям предоставляется возможность перемещаться по поеследовательности и читать её или меняя её элементы. Начало и конец таких последовательностей идентифицируются итераторами - объектами идентифицирующими элементы последовательности. 
![](https://i.imgur.com/bFUC7rx.png)

В стандартной библиотеке итератор начала указывает на первый элемент, а конца на элемент за последним в последовательности, т.е. по сути являются полуинтервалом (можно обозначать как `[begin,end)`).

**Основными свойствами всех итераторов являются:**
* Итератор указывает на элемент последовательности (или за последний)
* Два итератора сравнимы с помощью операторов `==` и `!=`
* Получить значение элемента, на который указывает итератор можно оператором `*`
* Итератор на следующий элемент можно получить через оператор `++`.

Рассмотрим пример функции поиска максимума из массива:
```cpp=
double find_max(const std::vector<double> &v) 
// return index of max element
{
    double h = -1;
    double *high = 0;
    for(size_t i = 0; i < v.size(); ++i) {
        if (h < v[i]) {
            high = &v[i];
            h = v[i];
        }
    }
    return high;
}
```

Эта же функция может быть написана с помощью итераторов:
```cpp=
template<typename Iterator>
Iterator find_max(Iterator first, Iterator last)
// return Iterator to max element
{
    Iterator max = first;
    for(Iterator tmp = first; tmp != last; ++tmp) {
        if (*max < *tmp) {
            max = tmp;
        }
    }
    return max;
}
```

Т.е. итераторы позволяют переписать методы так, что по сути становится без разницы каким контейнером пользоваться и какие данные от будет содержать в себе (конечно же при наличии оператора `<` у них).

Итераторы -- объекты, которые способны проходиться по элементам контейнера и предоставлять доступ к определенным элементам контейнера. У итераторов определены операторы `++/--` для перемещаться между элементами контейнера.

## Инвалидация итераторов

При работе с контейнером есть возможность добавлять элементы в него. Что в таком случае должно случиться с итератором на контейнер, если вставили элемент после инициализации итератора? Все зависит от контейнера.
В следующих контейнерах возможна инвалидация итераторов:
- `std::vector`
- `std::deque`
- `std::unordered_map`

В следующих контейнерах инвалидация итератора невозможна:
- `std::list`
- `std::map`

Все зависит от внутреннего устройства контейнера.
В случае удаления элемента итератор в любом контейнере инвалидируется

![](https://i.imgur.com/ZIh367y.png)


## Категории итераторов

Все итераторы можно разделить на следующие категории:

![](https://i.imgur.com/vRC50Wg.png)

* **Входной итератор (Input Iterator)** - можно перемещаться вперед через `++`, считывать значения через `*`, можно использовать только для одного прохода (далее может быть невалидным)
* **Выходной итератор (Output Iterator)** - можно перемещаться вперед через `++`, выводит значения через `*`
* **Однонаправленный итератор (Forward Iterator)** - можно перемещаться вперед через `++`, считывать или записывать элементы через `*`, можно использовать для многих проходов по контейнеру
* **Двунаправленный оператор (Bidirectional Iterator)** - можно перемещаться вперед через `++` и назад через `--`, можно считывать или записывать значения через `*`
* **Итератор призвольного доступа (Random Access Iterator)** - можно перемещаться вперед через `++`, назад `--`, доступ на чтение и запись через `*`. Доступен оператор `[]` для чтения и записи элементов. Можно складывать и вычитать с обычными числами, можно вычислить расстояние между двумя итераторами.

Описанное выше представимо в виде следующей таблицы:

![](https://i.imgur.com/1pQIKQZ.png)

## std next/advance/prev

Кроме стандартных операторов `++` и `!=`, `==` стандартная библиотека предлагает несколько функций для работы с итераторами:

* **next(iterator, n)** - возвращает новый итератор на n элементов вперед или назад (если n отрицательное) от переданного
```cpp=
template< class InputIt >
InputIt next( InputIt it, typename std::iterator_traits<InputIt>::difference_type n = 1 );

// use example 
std::vector<int> v{ 4, 5, 6 };
 
auto it = v.begin(); // it contains 4
auto nx = std::next(it, 2); // nx contains 6
std::cout << *it << ' ' << *nx << '\n';

it = v.end(); // it is after 6
nx = std::next(it, -2); // nx is now 2 step beforehead (5)
std::cout << ' ' << *nx << '\n';
```
* **advance(iterator, n)** - перемещает переданный итератор на n элементов, ничего не возвращает
```cpp=
template< class InputIt, class Distance >
void advance( InputIt& it, Distance n );

// use example
std::vector<int> v{ 3, 1, 4 };
 
auto vi = v.begin(); // vi contains 3
std::advance(vi, 2); // vi contains 4
std::cout << *vi << ' ';

vi = v.end(); // vi is after 4
std::advance(vi, -2); // vi contains 1
std::cout << *vi << '\n';
```
* **prev(iterator, n)** - возвращает новый итератор на n элементов назад или вперед (если n отрицательное) от переданного
```cpp=
template< class BidirIt >
BidirIt prev( BidirIt it, typename std::iterator_traits<BidirIt>::difference_type n = 1 );

// use example
 std::vector<int> v{ 3, 1, 4 };

auto it = v.end(); // it is after 4
auto pv = std::prev(it, 2); // pv contains 1
std::cout << *pv << '\n';

it = v.begin(); // it contains 3
pv = std::prev(it, -2); // pv contains 4
std::cout << *pv << '\n';
```
* **distance(first, last)** - вычисляет количество шагов, для выполнения условия `first == last`
```cpp=
template< class InputIt >
typename std::iterator_traits<InputIt>::difference_type
    distance( InputIt first, InputIt last );

// use example
std::vector<int> v{ 3, 1, 4 };
std::cout << "distance(first, last) = "
          << std::distance(v.begin(), v.end()) << '\n'
          << "distance(last, first) = "
          << std::distance(v.end(), v.begin()) << '\n';
           //the behavior is undefined (until C++11)
```

### Напишем свой advance

Попробуем реализовать простую версию алгоритма `advance`, который работает со смещением вперед.
На вход `my_advance` подается итератор и шаг смещения. Попробуем записать основу для функции
```cpp=
template <typename Iterator>
void my_advance(Iterator& iter, int n) {
    if (/* Check type of iterator */) {
        iter += n;
    } else {
        for (int i = 0; i < n; ++i) {
            ++iter;
        }
    }
}
```
Под условием проверки мы хотим вытянуть итератор с возможностью сразу переместиться на нужный шаг. Проверку можно осуществить с помощью шаблонной функции `is_same`
```cpp=
is_same<first_type, second_type>::value;
// Alternative version
is_same_v<first_type, second_type>;
```
Остается понять какие типы нужно сравнивать. И для этого случая стандартная библиотека имеет ответ -- `iterator_traits`. В `iterator_traits` устроен примерно следующим образом:
```cpp=
template <typename T>
struct iterator_traits {
  using value_type = ...;
  using iterator_category = ...;
}
```
`iterator_category` тип, который содержит категорию итератора. Он может быть одним из следующих типов:
- `input_iterator_tag`
- `forward_iterator_tag`
- `bidirectional_iterator_tag`
- `random_access_iterator_tag`
Каждый следующий тег является наследником предыдущего.

В такой реализации на вход `is_same` подается следующая конструкция
```cpp=
template <typename Iterator>
void my_advance(Iterator& iter, int n) {
    if (is_same_v<typename std::iterator_trits<Iterator>::iterator_category,
                  std::random_access_iterator_tag>) {
        iter += n;
    } else {
        for (int i = 0; i < n; ++i) {
            ++iter;
        }
    }
}
```
Такой код не может быть скомпилирован. Проблема в моменте компиляции. Если будет подан итератор, у которого нет оператора `+=`, то все упадет. Избежать этого поможет следующий шаблонный *helper*
```cpp=
template <typename Iterator, typename IterCategory>
void my_advance_helper(Iterator& iter, int n, IterCategory) {
    for (int i = 0; i < n; ++i) {
        ++iter;
    }
}

template <typename Iterator>
void my_advance_helper(Iterator& iter, int n, std::random_access_iterator_tag) {
    iter += n;
}

template <typename Iterator>
void my_advance(Iterator& iter, int n) {
    my_advance_helper(iter, n,
                      typename std::iterator_traits<Iterator>::iterator_category());
}
```

## const/reverse iterators

У итераторы по контейнерам могут иметь модификации. Одна из возможных модификаций это константный итератор. Его можно получить с помощью методов или же стандартных функций `std::cbegin()/std::cend()`. Возвращаемый итератор будет константным.
И еще одно из желаний это получить итератор, который в обратном порядке перебирает элементы. В стандартных контейнерах они доступны с помощью методов `std::rbegin()/std::rend()`.

#  Как написать свой итератор

В классе контейнере необходимо определить типы `iterator` и по желанию или необходимости `const_iterator` (типы нужны во-первых для удобства, а во-вторых без них не все стандартные алгоритмы будут работать), а также методы begin и end.

Для примера возьмем контейнер хранящий массив целых чисел.
```cpp=
class Integers
{
private:
    int m_data[100];
};
```

Обычно итераторы являются внутренними классами, поэтому объявим его внутри
```cpp=
class Integers
{
public:
    struct Iterator { /* ... */ };

    // ...
};
```

Стандартные алгоритмы могут ожидать от ваших итераторов несколько свойств:
1. **iterator_category** — одна из категорий итераторов. Список всех можно посмотреть [тут](https://en.cppreference.com/w/cpp/iterator/iterator_tags). Используем `std::bidirectional_iterator_tag`
2. **difference_type** — тип знакового числа, обозначающего расстояние между двумя итераторами этого типа. Наш итератор является оберткой над указателем, поэтому этим типом является `std::ptrdiff_t`
3. **value_type** — тип данных, содержащийся в контейнере. В нашем случае int
4. **pointer** — тип указателя на тип данных в контейнере. В нашем случае int*
5. **reference** — тип ссылки на тип данных в контейнере. В нашем случае int&

Объявим итератор с этими свойствами:
```cpp=
struct Iterator 
{
    using iterator_category = std::bidirectional_iterator_tag;
    using difference_type   = std::ptrdiff_t;
    using value_type        = int;
    using pointer           = int*;  // or also value_type*
    using reference         = int&;  // or also value_type&
};
```

Кроме того, каждый итератор должен удовлетворять следующим свойствам:
1. CopyConstructible (`T u = v`)
2. CopyAssignable    (`t = v`)
3. Destructible      (`u.~T()`)
4. Swappable         (`std::swap(u, v)`)

Запишем это:
```cpp=
struct Iterator 
{
    // Iterator tags here...

    Iterator(pointer ptr) : m_ptr(ptr) {}
    // rest of the requirements are default implemented
private:

    pointer m_ptr;
};
```

Так как наш итератор является двунаправленным, объявим необходимые операторы:
```cpp=
struct Iterator {

    // Iterator tags here...

    // Ctor...

    // pre increment
    Iterator& operator++();
    // post increment
    Iterator operator++(int);
    // pre decrement
    Iterator& operator--();
    // post decrement
    Iterator operator--(int);
    // dereference operator
    reference operator*();
    // dereference operator
    pointer operator->();

    friend bool operator!=(const Iterator& a, const Iterator& b);
    friend bool operator==(const Iterator& a, const Iterator& b);
private:
    pointer data_ptr;
};
```

В контейнере объявляем методы `begin()` и `end()`:
```cpp=
class Integers
{
public:

    // Iterator definition here ...

    // begin() point to the first element
    Iterator begin() { return Iterator(&m_data[0]); }
    // end() points past the last element, so it's idx is 100
    Iterator end()   { return Iterator(&m_data[100]); } 
};
```

Полная реализация класса
:::spoiler Спойлер
```cpp=
class Integers {
public:
    struct Iterator {
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = int;
        using pointer           = int*;
        using reference         = int&;

        Iterator(pointer ptr) : data_ptr(ptr) {}

        // pre increment
        Iterator& operator++() {
            data_ptr++;
            return *this;
        }
        // post increment
        Iterator operator++(int) {
            BidirectionalIterator tmp = *this;
            ++(*this);
            return tmp;
        }
        Iterator& operator--() {
            data_ptr--;
            return *this;
        }
        // post increment
        Iterator operator--(int) {
            BidirectionalIterator tmp = *this;
            --(*this);
            return tmp;
        }
        reference operator*() const {
            return *data_ptr;
        }
        pointer operator->() {
            return data_ptr;
        }

        friend bool operator!=(const Iterator& a, const Iterator& b) {
            return a.data_ptr != b.data_ptr;
        }
    private:
        pointer data_ptr;
    };

    Iterator begin() {
        return Iterator(&data[0]);
    }
    Iterator end() {
        return Iterator(&data[100]);
    }

    int operator[](int pos) const {
       return data[pos];
    }

    int& operator[](int pos) {
        return data[pos];
    }

private:
    int data[100];
};

int main() {
    // test if it works
    Integers ints{};
    Integers::BidirectionalIterator beg = ints.begin();
    for (auto end = ints.end(); beg != end; ++beg) {
        *beg = 10;
    }
    std::cout << "--------\n";
    beg = ints.begin();
    for (auto end = ints.end(); beg != end; ++beg) {
        std::cout << *beg << std::endl;
    }
}
```
:::

## std copy/find/find_if

В стандартной библиотеке существуют некоторые простые алгоритмы работы с итераторами.

Например, `copy`:
```cpp=
template< class InputIt, class OutputIt >
OutputIt copy( InputIt first, InputIt last, OutputIt d_first );
```

Пример использования:
```cpp=
#include <vector>
#include <algorithm> // for std::copy
void foo() {
    std::vector<int> from({1,2,3,4});
    for(auto item : from) {
        std::cout << item << ' ';
    }
    std::cout << std::endl;
    std::vector<int> to({5,6,7,8});
    std::copy(from.begin(), from.end(), to.begin());
    for(auto item : to) {
        std::cout << item << ' ';
    }
    std::cout << std::endl;
}
```

Например, `find` для поиска значения (возвращает итератор на первый найденный или `end()`):
```cpp=
template< class InputIt, class T >
InputIt find( InputIt first, InputIt last, const T& value );
```

Пример использования:
```cpp=
#include <vector>
#include <algorithm> // for std::find
void foo() {
    std::vector<int> v{1, 2, 3, 4};
    int n1 = 3;
    int n2 = 5;
 
    auto result1 = std::find(begin(v), end(v), n1); // iterator to 3
    auto result2 = std::find(begin(v), end(v), n2); // iterator == v.end()
}
```

# Задание

[Сортирующий итератор](https://hackmd.io/Smj-IqcpR6ajc2WqZzfVlQ)