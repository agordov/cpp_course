---
title: RAII
tags: cpp, abstract, 2sem
---

## RAII

**Resource Acquisition Is Initialization** -- способ поддержки программного кода. Принцип обращения с объектами, который указывает на обязательно освобождение ресурсов во время происхождения локальных ошибок.

Справка по [RAII](https://en.cppreference.com/w/cpp/language/raii).

## Правило трех

Если требуется реализовать копирующий конструктор/оператор присваивания с копированием/деструктор, то требуется реализация всех трех компонент. Пример:
```cpp=
#include <iostream>
#include <cstring> // memcpy

template <typename T>
struct Buffer {
  size_t size = 0;
  T* buffer = nullptr;
  
  // Constructor
  Buffer() : size(0), buffer(nullptr) {
    std::cout << "Buffer()\n";
  } 
  
  // Constructor
  Buffer(size_t size) : size(size), buffer(new T[size]) {
    std::cout << "Buffer(size_t)\n";
  } 
  
  // Copy constructor
  Buffer(const Buffer& other) : Buffer(other.size) {
    std::cout << "Buffer(const Buffer&  other)\n";
    // don't use memcpy for non-POD types
    memcpy(buffer, other.buffer, size * sizeof(T));
    // also possible instead of memcpy
    std::copy(other.buffer, other.buffer + other.size, buffer);
  }
  // OLD fashioned
  Buffer& operator=(Buffer& other) {
    std::cout << "Buffer& operator=(Buffer& other)\n";
    if (&other == this) {
      std::cout << "self copy\n";
      return *this;
    }
    // release old data
    delete [] buffer;
    // set new size
    size = other.size;
    // allocate new memory
    buffer = new T[size];
    // don't use memcpy for non-POD types
    memcpy(buffer, other.buffer, size * sizeof(T));
    // also possible instead of memcpy
    std::copy(other.buffer, other.buffer + other.size, buffer);
    return *this;
  }
  
  // NEW style (copy-and-swap)
  Buffer& operator=(Buffer& other) {
    std::cout << "Buffer& operator=(Buffer& other)\n";
    if (&other == this) {
      std::cout << "self copy\n";
      return *this;
    }
    Buffer tmp = other; // copy constructor
    std::swap(buffer, tmp.buffer);
    std::swap(size, tmp.size);
    // here tmp is destructed and it's resources are freed
    return *this;
  }  
    
  // Destructor
  ~Buffer() {
    std::cout << "~Buffer()\n";
    delete [] buffer;
  }
};


int main() {
  Buffer<int> int_buffer_empty;
  Buffer<int> int_buffer(5);
  Buffer<int> int_buffer_new(int_buffer);
  int_buffer = int_buffer;
  int_buffer_empty = int_buffer;
  return 0;
}

```
## Правило пяти

С появлением С++11 правило трех модифицировалось до правила пяти: если требуется реализовать одну из следующих компонент, то реализации требуют все пять
- Конструктор копирования
- Оператор присваивания с копированием
- Конструктор перемещения
- Оператор присваивания с перемещением
- Деструктор

### value

- `locator value/lvalue` -- объект, который имеет определенное место в памяти. Какая-либо ранее объявленная переменная.
- `rvalue` -- условно, это все то, что не является `lvalue`

В С++ много разных [value...](https://en.cppreference.com/w/cpp/language/value_category)

## Перемещения

Изначально С++ не позволял хранить в качестве переменных временные объекты (rvalue), но при этом позволял хранить их в виде констант.
```cpp=
int &a = 42; // CE
const int &b = 1337;
```

С течением времени в язык добавилась возможность хранить временные объекты с помощью специального синтаксиса:
```cpp=
std::string   s1     = "Hello ";
std::string   s2     = "world";
std::string&& s_rref = s1 + s2;    // the result of s1 + s2 is an rvalue
s_rref += ", my friend";           // I can change the temporary string!
std::cout << s_rref << '\n';       // prints "Hello world, my friend"
```
Примечание 1: Написание в виде ```std::string& s_rref = s1 + s2;``` невозможно, так как оператор сложения возвращает временный объект, а значит ссылка на него не может быть инициализирована без модификатора `const`

Изначально такая возможность может показаться бесполезной, но она позволяет избежать лишних копирований при работе с объектами. Наиболее показательны классы, работающие с динамически выделенными ресурсами.

Рассмотрим пример из предыдущего пункта:
```cpp=
#include <iostream>
#include <cstring> // memcpy

template <typename T>
struct Buffer {
  size_t size = 0;
  T* buffer = nullptr;
  
  // Constructor
  Buffer(size_t size) : size(size), buffer(new T[size]) {
    std::cout << "Buffer(size_t)\n";
  } 
  
  // Copy constructor
  Buffer(const Buffer& other) : Buffer(other.size) {
    std::cout << "Buffer(const Buffer&  other)\n";
    std::copy(other.buffer, other.buffer + other.size, buffer);
  }
  
  Buffer& operator=(Buffer& other) {
    std::cout << "Buffer& operator=(Buffer& other)\n";
    if (&other == this) {
      std::cout << "self copy\n";
    }
    Buffer tmp = other; // copy constructor
    std::swap(buffer, tmp.buffer);
    std::swap(size, tmp.size);
    // here tmp is destructed and it's resources are freed
    return *this;
  } 
    
  // Destructor
  ~Buffer() {
    std::cout << "~Buffer()\n";
    delete [] buffer;
  }
};


int main() {
  Buffer<int> int_buffer(5);
  return 0;
}
```

Если теперь возникнет ситуация когда нам понадобится функция следующего вида:
```cpp=
template<typename T>
Buffer<T> foo(size_t size) {
    return Buffer<T>(size);
}
```

Она возвращает объект по значению, а значит будет создан временный объект и потом он будет сокпирован. А так как наш буффер может быть огромным, то такие лишние копирования будут занимать много времени и лишних действий.

Избежать этого можно с помощью перемещений, создав конструктор перемещения и оператор перемещающего присваивания:
```cpp=
// move constructor
Buffer(Buffer&& other) : size(other.size), buffer(other.buffer) {
    std::cout << "Buffer(Buffer&&  other)\n";
    other.size = 0; // since temporary object has nullptr pointer
    other.buffer = nullptr; // otherwise it will cause double free
}

// move assignment operator
Buffer& operator=(Buffer&& other) {
    std::cout << "Buffer& operator=(Buffer& other)\n";
    if (&other == this) {
      std::cout << "self copy\n";
        return *this;
    }
    // free current data
    delete []buffer;
    // get the resources
    size = other.size;
    buffer = other.buffer;
    // set temporary object data to null
    other.size = 0;
    other.buffer = nullptr;
    
    return *this;
}
```

Основная идея заключается в том, что временный объект можно менять, а значит мы берем из него данные, а его самого делаем пустым.

**Примечание 1:** Если теперь попробовать запустить этот код, то вы заметите, что перемещение никогда не вызывается. Это происходит из-за оптимизаций компилятора, чтобы их отключить используйте флаг `-fno-elide-constructors` или надо явно указать, что требуется вызов перемещений следующим образом:
```cpp=
Buffer<int> buf(10);
// std::move transform lvalue into rvalue
Buffer<int> new_buf = std::move(buf);
```


## Правило нуля

Мартино Фернандес: “не определяйте самостоятельно ни одну из функций 5ки, вместо этого поручите заботу о владении ресурсами специально придуманным для этого классам”.
О таких классах поговорим в будущем (умные указатели).
