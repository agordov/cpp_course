---
title: Основы ООП. Классы и структуры
tags: cpp, abstract, 2sem
---

# Основы ООП. Классы и структуры

## Концепция классов

Созданные пользователем типы, объединяющие в себе некоторые данные и способы работы над ними называются классами.

Данные, которые будут храниться в этом классе называются **полями**, а функции, которые можно совершать над этим объектом называются **методами**. 


## Объявление классов, полей и методов

Имя класса должно начинаться с **заглавной** буквы.

Пример объявления класса:
```cpp=
#include <iostream>

// class declaration
class MyClass {
public:
    // fields
    int x;
    char *ptr;
    std::vector<int> v;
    
    // methods
    void print(int a) {
        std::cout << x * a << std::endl;
    }

};

int main() {
    MyClass m;
    m.print(10);
}
```

В строке 19 создастся экземпляр класса, для его создания необходимо проинициализировать все его поля *(x, ptr, v)*, в данном случае все они будут созданы и заполнены значениями по умолчанию.

Можно вручную указать как инициализировать поля по умолчанию:

```cpp=
#include <iostream>

// class declaration
class MyClass {
public:
    // fields
    int x = 42;
    char *ptr = nullptr;
    std::vector<int> v;
    
    // methods
    void print(int a) {
        std::cout << x * a << std::endl;
    }

};

int main() {
    MyClass m;
    m.print(10);
}
```

Методы можно объявлять в классе, а определять их позже:
```cpp=
class Foo {
    int x;
public:
    int getX();
};

int Foo::getX() {
    return x;
}
```

## Модификаторы доступа

В С++ существует несколько модификаторов доступа. В данный момент рассмотрим только два из них, а именно **public** и **private**.

По умолчанию внутри классов все поля и методы объявляются **private**, т.е. доступ к ним возможен только изнутри этого класса и классов друзей (будут разобраны позже). В структурах же наоборот все поля и методы по умолчанию являются **public**, т.е. к ним имеется доступ из любой точки программы.

В примере выше указан один модификатор доступа - **public**, без этого указания вызвать метод **print** невозможно.

Стоит заметить, что объявлений этих модификаторов может быть сколько угодно много внутри одного класса. Кроме того, объявление модификатора доступа распространяется на все поля и методы ниже его, до указания другого модификатора.

```cpp=
class Dog {
private:
    unsigned short age;
public:
    void Bark () {
        std::cout << " Bark " << std::endl ;
    }
private:
    std::string nickname;
public:
    static int dog_number;

    int getDogNumber() {
        return ++dog_number;
    }
};

```

Основное отличие класса от структуры:
```cpp=
struct Foo {
    int x;
    int foo() { return x;}
};
class Bar {
    int x;
    int bar() { return x;}
};

int main() {
    Foo f;
    Bar b;
    f.foo(); // OK all fields in struct are public by default
    b.bar(); // compilation error bc in class all fields are private by default
}
```

## Конструкторы, деструкторы

Конструктор - метод, определяющий как создавать класс (дополнительная логика).

По умолчанию компилятор сам создаст конструктор, который инициализирует поля (если известно как их инициализировать)

Синтаксис конструктора:
```cpp=
class MyClass {
    // empty constructor
    MyClass() {}
    
    // overloaded constructor
    MyClass(int a, int b) {
    
    }
};
```

```*.cpp=
class MyClass {
    int x = 0;
    char *p = nullptr;
    std::vector <v> str;
};
int main () {
    MyClass s; // OK
    MyClass ss = MyClass(); // OK
    MyClass sss(); // CE (it's function, not class)
}
```


Можно делать конструктор с параметрами:
```cpp=
class MyClass {
    int x = 0;
    char *p = nullptr;
    std::vector <v> v;
public:
    MyClass (int n) {
        x = n;
    }
};
int main() {
    MyClass s; // CE , default constructor was removed
    MyClass ss(5); // OK
    MyClass sss = MyClass (5); // OK
    MyClass ssss = 5; // OK
}
```

При определении кастомного конструктора дефолтный перестает быть доступным. (см. пример выше)


Пример конструктора с дополнительной логикой:
```cpp=
class String {
    private :
    char * str = nullptr;
    size_t sz = 0;
public :
    String (size_t sz, char ch) {
        // this == pointer to me , -> == (*p).
        this->sz = sz ; 
        str = new char[sz];
        // not really effective
        /* for ( size_t i = 0; i < sz; i ++) { 
            str[i] = ch;
        }*/
        memset(str, ch, sz); // nice
    }
};

int main() {
    String s(5, ’a’);
    String ss = s; // compile, but Shallow copy
}
```

### Конструктор копирования и оператор копирующего присваивания

Конструктор копирования - метод, позволяющий создать объекта данного класса из другого объекта данного класса.

По умолчанию также будет создан конструктор копирования, но с прямым копированием полей (shallow copy). 

```cpp=
class String {
private:
    char *str = nullptr;
    size_t sz = 0;
public:

    String(size_t sz, char ch) {
        this->sz = sz; // this == pointer to me , -> == (*p).
        str = new char[sz];
        memset(str, ch, sz);
    }
    // wrong , bc recursive copying / catched by compiler
    String(String s) {} 
    // wrong , bc can ’t use like String s = " abc ";
    String(String & s) {} 
    // right , but not nice - looking
    String (const String &s) { 
        sz = s.sz;
        str = new char[sz];
        memcpy(str, s.str, sz);
    }

    // Since C ++11
    String() = default;
    // to delete constructors and any functions
    String(String &) = delete; 

    // WRONG !!!!
    /* String(const String &s) {
        String(s.sz, 0);
        memcpy(str, s.str, sz);
    }*/

    // Assignment operator
    String& operator=(const String &s) { // can ’t use on itself
        delete []str;
        sz = s.sz;
        str = new char[sz];
        memcpy(str, s.str, sz);
        return *this;
    }

    // copy and swap
    void swap(String &s) {
        std::swap(str, s.str);
        std::swap(sz, s.sz);
    }

    String& operator=(const String &s) {
        if (this == &s) return *this;
        String tmp = s;
        swap(tmp);
        return *this;
    }

    // Since C ++11 can use initializer list
    String(const std::initializer_list<char> &lst) { 
        sz = lst.size();
        str = new char[sz];
        std::copy(lst.begin(), lst.end(), lst);
    }
};

int main () {
    String s;
    String s (5 , ’a’);
    String ss = s ; // compile , but deep copy
    String sss = {’a’, ’b’, ’c’}; // for initializer list
    String ssss {’a’,’b’,’c’}; // for initializer list
}
```

## Деструктор

Деструктор - специальный метод, вызывается при уничтожении объекта.

Деструктор нужен не для уничтожения полей, а дополнительная логика перед уничтожением полей. **Обязателен при ручном выделении памяти.**

Синтаксис деструктора:
```cpp=
class MyClass {
    
    MyClass() {
    
    }
    
    ~ClassName() {

    }
};
```

Пример деструктора:
```cpp=
class Foo { // not necessary custom destructor
    std::vector<int> v;
};

class Bar {
    int *v = nullptr;
    int sz;
public:
    Bar(int sz, int v) {
        v = new int[v];
        this->sz = sz;
    }
    
    ~Bar(){ // need for memory freeing
        delete []v;
    }
};

int main() {
}
```

### Порядок выполнения конструктора и деструктора

```cpp=
class A {
    A() { cout << "A";}
    ~A() { cout << "~A";}
};
class B {
    A a;
    B() { cout << "B";}
    ~B() { cout << "~B";}
};
int main() {
    B b; // print AB~B~A
    A a; // print ABA ~A~B~A // bc stack
}
```


## Типы конструкторов

```cpp
class MyClass {
    int x = 0;
    int *p = nullptr;
    // default constructor
    // ... some logic
};
class MyClass {
    int x;
    int *p;
public:
    MyClass(int x, int *p): x(x), p(p) {} // initializing constuctor
    // ... some logic
};

class MyClass {
    int x = 0;
    int *p = nullptr;
public:
    MyClass(): MyClass(10) {}
    MyClass(int x): MyClass (x, nullptr) {} // delegating constuctor
    MyClass(int x, int *p):x(x), p(p) {}
};
```

### Аггрегирующая инициализация

Работает в случае, если все поля являются публичными.

```cpp
struct S {
    int x;
    double y;
    std::string s;
};
int main() {
    S s = {1, 0.0, "abc"};
}
```

## Перегрузка операторов

### Оператор присваивания

Синтаксис оператора присваивания:

```cpp=
class MyClass {
public:
    MyClass& operator=(const MyClass &other) {}
}
```

### Правило трех

Если при описании класса потребовалось явно определить либо деструктор, либо копирующий конструктор, либо копирующий оператор присваивания, то надо определить все три.

### Операторы +, -, +=, -=

```cpp=
#include <iostream>
 
// +, +=, ++,
// first +=, then +
class Complex {
    double re = 0;
    double im = 0;
public:
    //Complex(double re) : Complex(re, 0) {}
    Complex(double re, double im = 0) : re(re), im(im) {}
 
    Complex(const Complex&) {
        std::cout << "copy" << std::endl;
    }
 
    Complex& operator+=(const Complex& c) { // 1st
        re += c.re;
        im += c.im;
        return *this;
    }
    // return value optimization (RVO)
    // must be outside class bc c+5.0 OK, but 5.0+c CE
    // actually look like c.operator+(5.0), but 5.0 don't have conversion to complex
    /*Complex operator+(const Complex& c) { // 2nd
        Complex sum = *this;
        sum += c;
        return sum;
    }*/
 
    // prefix increment
    Complex& operator++() {
        return *this += 1;  // bc have +=
    }
 
    // postfix increment
    // fictive int
    Complex operator++(int) {
        Complex copy = *this;
        ++*this;
        return copy;
    }
 
    // unary +-
    //Complex operator-() {}
 
};
// first +, then +=
/*class Complex {
    double re;
    double im;
public:
    Complex(double re, double im) : re(re), im(im) {}
 
    Complex(const Complex&) {
        std::cout << "copy" << std::endl;
    }
 
    Complex& operator+=(const Complex& c) { // 1st
        *this = *this + c; // double copy
        return *this;
    }
    // return value optimization
    Complex operator+(const Complex& c) { // 2nd
        Complex sum = *this;
        sum.re += c.re;
        sum.im += c.im;
        return sum;
    }
};*/
 
/*class Complex {
    double re;
    double im;
public:
    Complex(double x) : re(x), im(0.) {} // allow c + 5.0
    Complex(double re, double im) : re(re), im(im) {}
 
    Complex(const Complex&) {
        std::cout << "copy" << std::endl;
    }
 
    Complex& operator+=(const Complex& c) { // 1st
        re += c.re;
        im += c.im;
        return *this;
    }
 
};*/
 
Complex operator+(const Complex& a, const Complex& b) {
    Complex sum = a;
    sum += b;
    return sum;
}
 
 
int main() {
    Complex c(1., 2.);
    Complex cc(0, 1);
    // copy elision (since c++17)
    Complex cc = c + c; // how much copying?
}
```

# Ссылки

1. [Класс](https://en.cppreference.com/w/cpp/language/class)
2. [Конструкторы](https://en.cppreference.com/w/cpp/language/constructor)
3. [Копирующие конструкторы](https://en.cppreference.com/w/cpp/language/copy_constructor)
4. [Копирующее присваивание](https://en.cppreference.com/w/cpp/language/copy_assignment)
5. [Copy elision](https://en.cppreference.com/w/cpp/language/copy_elision)
6. [Деструктор](https://en.cppreference.com/w/cpp/language/destructor)
7. [Перегрузка операторов](https://en.cppreference.com/w/cpp/language/operators)
8. [Арифметические операторы](https://en.cppreference.com/w/cpp/language/operator_arithmetic)
