---
title: Умные указатели и Ко
tags: cpp, abstract, 2sem
---


# Perfect forwarding

Для конструкторов и некоторых функций может потребоваться вызов с аргументами, поэтому необходимо использовать следующий прием.

Пример:
```cpp=
class A {
    int x;
    double y;
public:
    A(int x, int y): x(x), y(y) {

    }
};

template<typename... Args>
A createA(Args&&... args) {
    return A(std::forward<Args>(args)...);
}

int main() {
    auto a = createA<int, double>(42, 3.14);
}
```

`forward<Args>(args)` позволяет передать аргументы без искажения их типов, без лишних копирований и т.д.

# Предпосылки использования умных указателей

1. Объявление указателя не говорит о том, на один объект или массив объектов он указывает
2. Нет информации о том, надо ли уничтожать то, на что указывает указатель или нет после завершения вашего кода
3. Если определено, что ваш участок кода должен уничтожить объект, то нет однозначности делать это посредством вызова `delete` или специальным способом
4. Использовать `delete` или `delete[]`?
5. Нет способа проверить первый ли раз происходит удаление указателя (двойное освобождение приводит к неопределенному поведению)
6. Нет возможности проверить является ли указатель висячим, т.е. указывает на уже освобожденный объект или нет
7. Излишнее раздувание кода. (см. Пример 1)

Пример 1:
```cpp=
template<typename T1, typename T2, typename T3>
int foo(size_t n) {
    T1 *a = new T1;
    T2 *b = new T2;
    T3 *c = new T3[10];
    /*
     * 
     *  some logic with a, b, c
     * 
     * */
    if (n % 3 == 0) {
        delete a;
        delete b;
        delete []c;
        return 2;        
    } else if (n % 3 == 1) {
        delete a;
        delete b;
        delete []c;
        return 1;
    } else {
        delete a;
        delete b;
        delete []c;
        return 0;
    }
}
```

Для решения этих проблем были созданы умные указатели - `auto_ptr`(deprecated), `unique_ptr`, `shared_ptr`, `weak_ptr`.

Указатель `auto_ptr` является устаревшим и более не используется с С\++11, т.к. его заменил `unique_ptr`.

# Unique_ptr

Умный указатель `unique_ptr` является воплощением *исключающего владения*, т.е. ненулевой `unique_ptr` всегда владеет тем, на что указывает. Перемещение его при этом передает владение от исходного к целевому (исходный становится нулевым). Копирование `unique_ptr` запрещено, т.к. в этом случае появится два владельца одного ресурса, что приведет к ошибкам освобождения (каждый считает, что он владелец). При вызове деструктора `unique_ptr` освобождает владение ресурсом.

Пример простой реализации `unique_ptr`:
```cpp=
template<typename T>
class Unique_ptr {
    T*   data;
public:
    Unique_ptr(T* data)
        : data(data)
    {}
    Unique_ptr(const Unique_ptr&) = delete;
    Unique_ptr& operator=(const Unique_ptr&) = delete;
    Unique_ptr(Unique_ptr&&);
    Unique_ptr& operator=(const Unique_ptr&&);
    ~Unique_ptr() {
        delete data;
    }
    T* operator->() {return data;}
    T& operator*()  {return *data;}
    T* release() {
        T* result = nullptr;
        std::swap(result, data);
        return result;
    }
    // So it can be used in conditional expression
    operator bool() {return data;}
};
```

Минусы данной реализации? (подумайте сами, потом сравните с ответом)
:::spoiler Ответ
1. Неявный конструктор. Следующий код скорее всего приведет к ошибкам освобождения:
```cpp=
void takeOwner1(Unique_ptr<int> x)
{}
void takeOwner2(Unique_ptr<int> const& x)
{}
void takeOwner3(Unique_ptr<int>&& x)
{}
int main()
{
    int*   data = new int(7);

    takeOwner1(data);
    takeOwner2(data);
    takeOwner3(data);
}
```
2. Время жизни объекта при передаче указателя в конструктор не зависит от самого класса умного указателя. Пример:
```cpp=
takeOwner1(data);

// You can think of this as functionally equivalent to:

{
    Unique_ptr<int> tmp(data);
    takeOwner1(tmp);
}
```
3. Разыменование нулевого указателя в `operator*` и `operator->` (см. [Оператор ->](https://stackoverflow.com/questions/8777845/overloading-member-access-operators))
4. Константность операторов `operator*` и `operator->`
5. Оператор приведения к `bool` неявный, т.е теперь можно сравнивать через `==`, что неверно
:::

## make_unique

Передача прямого указателя в конструктор может приводить к потенциальным ошибкам, поэтому для уменьшения шанса возникновения таких случаев следует использовать специальную функцию `make_unique` ([make_unique](https://en.cppreference.com/w/cpp/memory/unique_ptr/make_unique)).

Примерная реализация функции `make_unique`
```cpp=
template<typename T, typename... Args>
unique_ptr<T make_unique(Args&&... argd) {
    return unique_ptr<T>(new T(forward<Args>(args)...));
}
```

Пример как не надо писать:
```cpp=
foo(std::unique_ptr<T>(new T), bar());
```
:::spoiler Пояснение
До стандарта С\++14 включительно не было однозначности в порядке вычислений аргументов функций, поэтому сначала могло быть выполнено `new T`, затем `bar()`, которое могло прервать выполнение программы и вызвать утечку памяти. Со стандарта С++17 гарантируется, что вычисление аргументов не может пересекаться.

Но можно написать так и снова всё сломать :)
```cpp=
foo(unique_ptr<T> ptr, int *p) {
    if (p) {
        record(*p);
        delete p;
    }
}

foo(unique_ptr<T>(new T), new int {10});
```
:::

# shared_ptr

В отличие от `unique_ptr`  данный умный указатель не владеет самим объектом, но позволяет организовать совместное владение. Т.е. когда данный объект не будет более нужен он уничтожится. Это может произойти в результате того, что последний `shared_ptr`, указывающий на данный объект будет разрушен или если последний владелец поменяет указатель на объект. Читать подробнее [тут](https://en.cppreference.com/w/cpp/memory/shared_ptr)

Указатель `shared_ptr` определяет является ли он владельцкм посредством счетчика, т.е. в конструкторах он почти всегда будет увеличиваться, а в деструкторах уменьшаться.

Пример простой реализации `shared_ptr`:
```cpp=
template <typename T>
class shared_ptr {
private:
    T* ptr = nullptr;
    //size_t count; cant have bc have to increase all other
    size_t* counter = nullptr;
public:
    shared_ptr() {}
 
    shared_ptr(T* ptr) : ptr(ptr), counter(new size_t(1)) {}
 
    shared_ptr(const shared_ptr& other) : ptr(other.ptr), counter(other.count) {
        ++*counter;
    }
 
    shared_ptr(shared_ptr&& other) :
        ptr(other.ptr), counter(other.counter) {
        other.ptr = nullptr;
        other.counter = nullptr;
    }
 
    shared_ptr& operator=(const shared_ptr& another) {
        if (another.ptr != ptr) {
            ptr = another.ptr;
            another.ptr = nullptr;
        }
    }
 
    shared_ptr& operator=(shared_ptr&& another) noexcept {
        delete ptr;
        ptr = another.ptr;
        another.ptr = nullptr;
    }
 
    T& operator*() {
        return *ptr;
    }
 
    T* operator->() {
        return ptr;
    }
 
    size_t use_count() const {
        return *counter;
    }
 
    ~shared_ptr() {
        if (*counter > 1) {
            --*counter;
            return;
        }
        delete ptr;
        delete counter;
    }
};
```

Какие проблемы есть в этой реализации?
:::spoiler Ответ
1. Опять конструктор от указателя
2. Константность методов...
3. Еще много других проблем
:::

## make_shared_ptr

Для создания разделяющего владение умного указателя аналогично функции `make_unique` существует функция `make_shared`. Читать подробнее [тут](https://en.cppreference.com/w/cpp/memory/shared_ptr/make_shared)

Вариант реализации функции `make_shared`:
```cpp=
template <typename T, typename... Args>
shared_ptr<T> make_shared(Args&&... args) {
    auto ptr = new T(std::forward<Args>(args)...);
    return shared_ptr<T>(ptr);
}
```

### Пример

```cpp=
#include <iostream>
#include  <memory> // for shared_ptr
int main()
{
    // Creating a shared_ptr through make_shared
    std::shared_ptr<int> p1 = std::make_shared<int>();
    *p1 = 78;
    std::cout << "p1 = " << *p1 << std::endl;
    // Shows the reference count
    std::cout << "p1 Reference count = " << p1.use_count() << std::endl;
    // Second shared_ptr object will also point to same pointer internally
    // It will make the reference count to 2.
    std::shared_ptr<int> p2(p1);
    // Shows the reference count
    std::cout << "p2 Reference count = " << p2.use_count() << std::endl;
    std::cout << "p1 Reference count = " << p1.use_count() << std::endl;
    // Comparing smart pointers
    if (p1 == p2)
    {
        std::cout << "p1 and p2 are pointing to same pointer\n";
    }
    std::cout<<"Reset p1 "<<std::endl;
    p1.reset();
    // Reset the shared_ptr, in this case it will not point to any Pointer internally
    // hence its reference count will become 0.
    std::cout << "p1 Reference Count = " << p1.use_count() << std::endl;
    // Reset the shared_ptr, in this case it will point to a new Pointer internally
    // hence its reference count will become 1.
    p1.reset(new int(11));
    std::cout << "p1  Reference Count = " << p1.use_count() << std::endl;
    // Assigning nullptr will de-attach the associated pointer and make it to point null
    p1 = nullptr;
    std::cout << "p1  Reference Count = " << p1.use_count() << std::endl;
    if (!p1)
    {
        std::cout << "p1 is NULL" << std::endl;
    }
    return 0;
}
```

## shared_ptr*

В полях класса хранятся два указателя, которые скорее всего находятся в совершенно разных участках памяти, что усложняет доступ к ним. Исправить это можно с помощью специальной внутренней структуры `ControlBlock`, которая будет заниматься управлением счетчика/объекта и выделением/освобождением памяти под них. Т.к. они буду расположены внутри этой структуры, то гарантируется их расположение в памяти рядом. (см. [implementation notes](https://en.cppreference.com/w/cpp/memory/shared_ptr))

# weak_ptr

Данный умный указатель идёт в комплекте с `shared_ptr` и решает проблему висячих ссылок. Например, один `shared_ptr` может указывать на другой, а тот на первый (легко создать подобную связь в контейнерах (деревья/двунаправленные списки/и т.д.)). Данный *слабый* указатель позволяет решить эту проблему тем, что следит за указателем (счетчик) и сигнализирует, если он был освобожден.

Подробнее [тут](https://en.cppreference.com/w/cpp/memory/weak_ptr)

## Пример

```cpp=
#include <iostream>
#include <memory> // weak_ptr

int main()
{
    // OLD, problem with dangling pointer
    // PROBLEM: ref will point to undefined data!

    int* ptr = new int(10);
    int* ref = ptr;
    delete ptr;

    // NEW
    // SOLUTION: check expired() or lock() to determine if pointer is valid

    // empty definition
    std::shared_ptr<int> sptr;

    // takes ownership of pointer
    sptr.reset(new int);
    *sptr = 10;

    // get pointer to data without taking ownership
    std::weak_ptr<int> weak1 = sptr;

    // deletes managed object, acquires new pointer
    sptr.reset(new int);
    *sptr = 5;

    // get pointer to new data without taking ownership
    std::weak_ptr<int> weak2 = sptr;

    // weak1 is expired!
    if(auto tmp = weak1.lock())
        std::cout << "weak1 value is " << *tmp << '\n';
    else
        std::cout << "weak1 is expired\n";
    
    // weak2 points to new data (5)
    if(auto tmp = weak2.lock())
        std::cout << "weak2 value is " << *tmp << '\n';
    else
        std::cout << "weak2 is expired\n";
}
```
