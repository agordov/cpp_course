---
title: exceptions
tags: cpp, abstract, 2sem
---

# Идея исключений

Во время исполнения кода могут возникать ситуации, когда необходимо понять верно ли отработала функция или метод. Для этого можно воспользоваться например, кодами возврата.

```cpp=
enum error_t { E_OK = 0, E_NO_MEM, E_UNEXPECTED }

error_t open_file(const char *filename, FILE *handle);
```

Проблема данного подхода в том, что никто не проверяет коды возврата.

Но кроме того, не все функции предполагают такую возможность, например:
```cpp=
Matrix operator+(const Matrix& lha, const Matrix& rha);
```

В данный оператор некуда добавить код возврата.

Кроме того, подобная обработка ошибок приводит к необходимости писать код в следующем стиле:
```cpp=
error_t foo();

void bar() {
    int *a;
    // some logic on a
    if(foo()) {
        // free a
    }
    int *b;
    // some logic on b
    if(foo()) {
        // free a
        // free b
    }
    // ...
}
```

Или вводить методы проверки согласованности класса в него, что опять приводит  к необходимости постоянной проверки внутреннего состояния объекта. А кроме того, неизвестно что делать с таким объектом дальше.

# Виды ошибок

* Логические ошибки - ошибка разработчика (выход за границы массива, деление на 0, обращение по 0 указателю, segmentation fault и т.д.)
* Исключительные ситуации - требуется специальная обработка (ошибка записи файла, недоступность сервера, undefined page fault и т.д.)

Кроме того, исключительные ситуации предполагают возможное восстановление работы программы.

# Исключения в С++

Допустим написан следующий код:

```cpp=
struct UnwShow {
    UnwShow() { std::cout << "ctor\n"; }
    ~UnwShow() { std::cout << "dtor\n"; }
};

void foo(int n) {
    UnwShow s;
    if (n == 0) std::exit(-1); // call to exit programm
    foo(n - 1);
}

int main() {
    foo(4);
}
```

В этом случае будет напечатано 
```cpp=
ctor
ctor
ctor
ctor
ctor
```

Однако данный вызов можно обернуть в специальный блок кода (`try-catch`) и вместо выхода из всей программы продолжить её работу.

```cpp=
struct UnwShow {
    UnwShow() { std::cout << "ctor\n"; }
    ~UnwShow() { std::cout << "dtor\n"; }
};

void foo(int n) {
    UnwShow s;
    if (n == 0) throw 1; // call to exit programm
    foo(n - 1);
}

int main() {
    try {
        foo(4);
    } catch (int n) {
        std::cout << "Catched " << n << std::endl;
    }
}
```

В этом случае вывод программы будет
```cpp=
ctor
ctor
ctor
ctor
ctor
dtor
dtor
dtor
dtor
dtor
Catched 1
```

## Синтаксис исключений

Исключения необходимо "бросать" (`throw`). Бросать можно что угодно. При броске происходит создание объекта броска и размотка стека.

Пример:

```cpp=
throw 1;
throw new int(1);
throw SomeClass(1);
throw std::runtime_error
```

## Синтаксис ловли исключений

Для ловли исключений используется `catch` блок. В этом блоке можно что-либо сделать с исключением.

```cpp=
try {
    // some code that throws
} catch (ExceptionType exc) {
    // some work with exception
}
```

### Правила ловли исключений

* Ловится точный тип (неявные преобразования запрещены, пользовательские преобразования типов запрещены)
```cpp=
try { throw 1; } catch (long long n)  { } // not catched
```
* Ловится ссылка на точный тип
```cpp=
try { throw 1; } catch (const int &n) { } // catched
```
* Ловится указатель на тип
```cpp=
try { throw new int(1); } catch (int* n)       { } // catched
```
* Ловится ссылка или указатель на базовый класс (без ссылки производится "срезка" до базового объекта)
```cpp=
class Base {};
class Derived : public Base {};
try { throw Derived(); } catch (Base& b)  { } // catched
```

Проверка типов брошенных исключений с типом в `catch` производится в порядке следования этих блоков.

```cpp=
try { 
    throw 1; 
} catch (long long n) { 
    // not catched
} catch (int n) {
    // catched
}
```

Брошенные объекты можно менять

```cpp=
try { 
    throw new int(42); 
} catch (int* n) { 
    delete n; // possible memory leak
}
```

Пойманные исключения можно перевыбрасывать

```cpp=
try { 
    throw new int(42); 
} catch (int* n) { 
    // don't know what to do with this
    throw; // rethrow
}
```

## Стандартные исключения

В стандартной библиотеке С++ существуют уже созданные ранее исключения (подключаются через `<stdexcept>`). Правилом хорошего тона будет наследоваться от них и реализовать свою логику. Список всех стандартных исключений можно найти [тут](https://en.cppreference.com/w/cpp/error/exception)

Общее описание базового класса `exception` выглядит следующим образом:
```cpp=
class exception {
    exception() noexcept;
    exception(const exception&) noexcept;
    exception operator=(const exception&) noexcept;
    virtual ~exception();
    virtual const char * what();
};
```

`noexcept` - означает, что функция не выбросит исключения (если выбросит, то сразу вызовется `terminate`).

Однако, есть тут и подводный камень:
```cpp=
#include <iostream>
#include <stdexcept>

struct my_exc1 : std::exception {
  char const *what() const noexcept override { return "exc1"; }
};

struct my_exc2 : std::exception {
  char const *what() const noexcept override { return "exc2"; }
};

struct your_exc3 : my_exc1, my_exc2 {};

int main() {
  try {
    throw your_exc3();
  } catch (std::exception const &e) {
    std::cout << e.what() << std::endl;
  } catch (...) { // special syntaxis to catch any exception
    std::cerr << "whoops!\n";
  }
}
```

Это похоже на проблему ромба, но что будет, если сделать наследование виртуальным? (проверьте сами)

## Перехват всех исключений

В С++ существует способ ловли всех исключений

```cpp=
try {
    
} catch (...) {
    
}
```

## Function-try исключения

Для ловли исключений в труднодоступных местах используется синстаксис function-try-exception

Пример с неуловимым исключением
```cpp=
template<typename T> struct Foo {
    T x_, y_;
    Foo(T x, T y): x_(x), y_(y) { // <- possible exception in x_(x), y_(y)
        try {
            // some code
        } catch (std::exception &exc) {
            // some code
        }
    }
};
```
Исправлено
```cpp=
template<typename T> struct Foo {
    T x_, y_;
    Foo(T x, T y) try: x_(x), y_(y) { // <- possible exception in x_(x), y_(y)
        // ctor body
    } catch (std::exception& exc) {
        
    }
};
```

Аналогично можно обернуть всю функцию

```cpp=
int foo(int x) try {
    // func body
    
} catch (std::exception& exc) {
    x++; // possible
}
```

### Пример Каргилла

Рассмотрим следующий пример

```cpp=
template<typename T> class MyVector {
    T *arr_ = nullptr;
    size_t size_, used_ = 0;
public:
    MyVector(const MyVector& rhs) {
        arr_ = new T[rhs.size_]; // can throw bad alloc but it's ok
        size_ = rhs.size_; used_ = rhs.used_;
        for (size_t i = 0; i != size_; ++i) {
            arr_[i] = rhs.arr_[i]; // can throw
        }
    }
}; 
```

Гипотетически копирование может вызвать исключение. В этом случае деструктор вызван не будет и произойдет утечка памяти.

```cpp=
MyVector(const MyVector& rhs) {
    arr_ = new T[rhs.size_]; // can throw bad alloc but it's ok
    size_ = rhs.size_; used_ = rhs.used_;
    try {
        for (size_t i = 0; i != size_; ++i) {
            arr_[i] = rhs.arr_[i]; // can throw
        }
    } catch (...) {
        delete []arr_;
        throw;
    }
} 
```

## Гарантии безопасности исключений

Существует три основные гарантии безопасности исключений:
* Базовая - при возникновении любого исключения, состояние программы должно оставаться согласованным (т.е. схраняются инварианты классов и нет утечек ресурсов)
* Строгая - если при выполнении операции возникает исключение, то это не должно оказать какого-либо влияния на состояние приложения, т.е. возникает "атомарность" операции (либо она выполнена, либо нет)
* Отсутствия - и при каких обстоятельствах функция не будет генерировать исключения

## Исключения в деструкторах

По умолчанию все деструкторы помечены как `noexcept(true)` и в них нельзя кидать исключения. 

В деструкторах нельзя кидать исключения.

## noexcept

Оператор `noexcept(expression)` производит проверку времени компиляции выражения `expression` и в соответствии с результатом помечает функцию/метод как `noexpect(true)` или `noexcept(false)`.

Удобно использовать в следующем виде:
```cpp=
template<class T>
void do_math() noexcept(noexcept(std::declval<T>() + std::declval<T>()) && 
                        std::is_nothrow_copy_constructible<T>::value &&
                        std::is_nothrow_assignable<T&, T>::value)
{
    // some math
}
```

Данное выражение проверяет имеет ли тип `T` оператор сложения, присваивания и может ли быть сконструирован копированием.

**Замечание:** оператор `noexcept` не вычисляет явно выражение, а только оценивает его

Пример работы оператора `noexcept` [тут](https://godbolt.org/z/W7rojfoMf)

## Размотка стека

При выбрасывании исключений происходит размотка стека. Можно это проверить с помощью следующего [кода](https://godbolt.org/z/6Gqahf4KE)

```cpp=
#include <iostream>

struct UnwShow {
  int n_;
  long t = 0xDEADBEEF;
  UnwShow(int n) : n_(n) { std::cout << "ctor: " << n_ << "\n"; }
  ~UnwShow() { std::cerr << "dtor: " << n_ << "\n"; }
};

void foo(int n) {
  UnwShow s{n};

  // odr-use to materialize
  std::cout << "Addr of " << n << ": " << &s << std::endl;
  if (n == 0) {
    std::cout << "throw\n";
    throw 1;
  }
  foo(n - 1);
}

int main() {
  try {
    foo(5);
  } catch (int n) {
    std::cout << "catch\n";
  }
}
```

# Неочевидные ошибки

Данный вариант первого вектора в STL был незамеченным 6 лет (до 1998).

```cpp=
T& T::operator=(const T& x) {
    if (this != &x) {
        this->~T();
        new (this) T(x); // exc here
                         // dtor in unwinding
    }
    return *this;
}
```