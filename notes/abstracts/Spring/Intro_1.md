---
title: Введение
tags: cpp, abstract, sem1
---

# Семинар 1. Основы синтаксиса
Язык С++ является компилируемым, то есть трансляция кода с языка высокого уровня на инструкции машинного кода происходит не в момент выполнения, а заранее — в процессе изготовления так называемого исполняемого файла (в ОС Windows такие файлы имеют расширение .exe, а в ОС GNU/Linux чаще всего не имеют расширения).

Для компиляции необходим компилятор, который необходимо установить на вашей рабочей станции. Также стоит установить интегрированную среду разработки, которая включает в себя и удобный текстовый редактор с подсветкой синтаксиса, и удобные интерфейсы запуска компиляции и отладки, а также даёт подсказки при написании кода.
## Объявление переменных. Понятие инициализации и деструктурированного присваивания
Для начала стоит определить что такое переменная. Переменная - именованная область памяти. 
В языке С++ для работы с переменными и другими существует несколько видов взаимодействия.
Первый вид взаимодействия - объявление.
Пример объявления переменной:
```cpp=
int a;
char b;
```    
Следующий вид взаимодействия - определение, т.е. присвоение объявленной переменной какого-либо значения.
Пример определения переменной:
```cpp=
int a; // declaration
int b; // declaration
a = 10; // definition
b = 42; // definition
```
И последний вид взаимодействия - инициализация, которая сочетает в себе объявление и определение. Инициализация позволяет сконструировать переменную во время её объявления.
Пример инициализации переменной:
```cpp=
int a = 10;
char b = 42;
int  c = d = -1;
```
Подробнее почитать про инициализацию, определение и объявление можно тут: [объявление](https://en.cppreference.com/w/cpp/language/declarations), [определение](https://en.cppreference.com/w/cpp/language/definition), [инициализация](https://en.cppreference.com/w/cpp/language/initialization)

Со стандарта `С++17` стало возможно деструктурированное присваивание как в Python и некоторых других языках.
Пример деструктурированного присваивания:
```cpp=
int a[2] = {1,2};
auto [x,y] = a; // creates e[2], copies a into e, then x refers to e[0], y refers to e[1]
auto& [xr, yr] = a; // xr refers to a[0], yr refers to a[1]
```
Подробнее почитать про деструктурированное присваивание: [тут](https://en.cppreference.com/w/cpp/language/structured_binding)
# Фундаментальные типы
Основными типами C++ являются типы встроенные в сам язык, к таким типам относятся целочисленные типы, числа с плавающей точкой, логический тип, символьные типы и специальный пустой тип.
Пример использования фундаментальных типов
```cpp=
int a = 10;
unsigned int x = 0x1234; // hexadecimal value
bool flag = true;
char b = 'a';
void c; \\ Error
long long int d {10000000000};
```
Таблицу всех фундаментальных типов можно посмотреть [тут](https://en.cppreference.com/w/cpp/language/types)
## Ключевое слово auto
В некоторых случаях полное указание типа может представлять нетривиальную задачу, или указание типа не требуется явно. В этих и некоторых других случаях можно (и даже нужно) использовать ключевое слово `auto`. С помощью него компилятор самостоятельно может вывести тип, который требуется присвоить той или иной переменной.
Пример использования ключевого слова auto:
```cpp=
auto a = 1 + 2; // type of a is int
auto b = a;     // type of c0 is int, holding a copy of a
auto c = 1.0;   // type of c is double
auto m{5};  
```
Подробнее про `auto` - https://en.cppreference.com/w/cpp/language/auto
## Ключевое слово const
Для некоторых переменных может требоваться их неизменяемость, например, для констант типа `pi, e`. В этом случае используется ключевое слово `const`
Пример использования ключевого слова `const`:
```cpp=
int n1 = 0;           // non-const object
const int n2 = 0;     // const object
int const n3 = 0;     // const object (same as n2)
n1 = 1;               // ok
n2 = 2;               // error
```
Подробнее про `const` - https://en.cppreference.com/w/cpp/language/cv

## Блочная структура кода и области видимости
Вся программа состоит из набора инстукций. Инструкции, это фрагменты программы С++, которые выполняются последовательно. Тело любой функции представляет собой последовательность инструкций.
Пример инструкций:
```cpp=
int main() {
    int n = 1;                        // declaration statement
    n = n + 1;                        // definition statement
    std::cout << "n = " << n << '\n'; // statement
    return 0;                         // exit statement
}
```
Составные операторы или блоки, это последовательности инструкций, заключённые в фигурные скобки.
Когда ожидается одно условие, но необходимо выполнить несколько инструкций последовательно (например, в операторе if или в цикле), можно использовать составные операторы:
Пример составного блока:
```cpp=
if (x > 5) {
    int n = 1;
    std::cout << n;
}
```
Каждое имя, которое появляется в программе на C++, действительно только в некоторой, возможно, несмежной части исходного кода, называемой его областью видимости.

Потенциальная область видимости переменной, представленной объявлением в блоке (составной оператор), начинается в точке объявления и заканчивается в конце блока.

Фактическая область видимости такая же, как и потенциальная область, если только не существует вложенного блока с объявлением, которое вводит идентичное имя (в этом случае вся потенциальная область видимости вложенного объявления исключается из области видимости внешнего объявления)
Пример областей видимости:
```cpp=
int main() {
    int i = 0; // scope of outer i begins
    ++i; // outer i is in scope
    {
        int i = 1; // scope of inner i begins,
                   // scope of outer i pauses
        i = 42; // inner i is in scope
    } // block ends, scope of inner i ends,
        // scope of outer i resumes
} // block ends, scope of outer i ends
    //int j = i; // error: i is not in scope
```
Подробнее про область видимости - https://en.cppreference.com/w/cpp/language/scope
Подробнее про инструкции - https://ru.cppreference.com/w/cpp/language/statements
## Условные конструкции if / else
Для ветвления в работе программы используются условные операторы `if-else`. Главным отличием условных операторов в C++ от Python является то, что `else` относится к последнему `if'у`.
Синтакс условного оператора (упрощенный):
```cpp=
if (condition) [else]
```
Пример использования условного оператора:
```cpp=
int a = 10;
if (a < 20) {
    a += 10;
} else {
    a -= 10;
}
```
Пример использования условного оператора:
```cpp=
int a = 10;
if (a < 20) {
    if (a < 10) {
        a *= 5;
    } else {
        a /= 2;
    }
} else {
    a -= 10;
}
```
Подробнее про условные операторы - https://en.cppreference.com/w/cpp/language/if
Кроме `if-else` в языке C++ также есть условный оператор `switch`, который позволяет передать управление в одну из веток условий в зависимости от значения условия.
Синтаксис `switch`
```cpp=
switch (condition) {
    [case : statement;[break];]
    [default: statement;]
}
```
Пример использования `switch`:
```cpp=
int a = 10;
switch (a % 3) {
    case 1: a -= 1; break;
    case 2: a += 1; break;
    default: a = 0;
}
```
Ключевое слово `break` позволяет прекратить выполнение условного оператора `switch` и перейти к следующим за ним иструкциям.
## Циклы for, while, do / while
В языке C++ существует несколько видов циклов.

Самым простым циклом является цикл `while`. Такой цикл выполняет указанные действия до тех пор, пока условие цикла будет верно, в основном используется в тех случаях, когда заранее неизвестно сколько раз должен выполниться цикл. Условие цикла проверяется до его начала.
Синтаксис цикла `while`:
```cpp=
while (condition) statement
```
Пример цикла `while`:
```cpp=
int a = 10;
int b = 1;
while (a > 0) {
    b *= a;
    a--;
}
```
Подробнее про цикл `while` - https://en.cppreference.com/w/cpp/language/while

Вариацией цикла `while` является цикл `do-while`. Отличие этого цикла состоит в том, что выражение внутри цикла всегда будет выполнено хотя бы раз. Проверка условия цикла происходит после выполнения очередного выражения
Синтаксис цикла `do-while`:
```cpp=
do statement while (condition);
```
Пример цикла $while$:
```cpp=
int a = 10;
int b = 1;
do {
    b *= a;
    a--;
} while (a > 0);
```
Подробнее про цикл `do-while` - https://en.cppreference.com/w/cpp/language/do

Цикл `for` используется в тех случаях когда заранее известно сколько раз должен выполниться цикл.
Синтаксис цикла `for`:
```cpp=
for (init-statement; iteration condition; post iteration expression) statement
```
Пример цикла `for`:
```cpp=
int a = 1;
for (int i = 0; i < 10; ++i) {
    a *= i;
}
```
Пример цикла `for`:
```cpp=
int a = 1;
int i = 0;
for (; i < 10; ++i) {
    a *= i;
}
```
Подробнее про цикл `for` - https://en.cppreference.com/w/cpp/language/for
## Операторы
Подробнее про порядок выполнения операторов -https://en.cppreference.com/w/cpp/language/operator_precedence