---
title: Память
tags: cpp, abstract, sem1
---

# Типы памяти в C++
Существует три типа памяти: автоматическая, динамическая, статическая. 
![](https://i.imgur.com/JyX0wib.jpg)
Автоматическая - выделяется на стеке и ограничена его размером
```cpp=
int array[100];
```
Динамическая - выделется пользователем на куче
```cpp=
int n = 10;
int* array = new int[n];
delete[] array;
```
Статическая - выделяется до запуска main, отсюда вытекает, что создается вначале и уничтожается в конце программы
```cpp=
static int x = 0;
```

Пример переполнения стека:
```cpp=
int main() {
    int array[1000][1000]; // stack overflow
}
```
Пример переполнения кучи:
```cpp
int main() {
    for (int i = 0; i < 10000000; i++) {
        int a = new int[10000000];
    }
}
```
Выделить память из кучи можно с помощью оператора `new`. Так как происходит ручное выделение памяти, то и заниматься её очисткой нужно вручную - этим занимается оператор `delete`.

Пример выделения памяти для массива из кучи и стека:
```cpp=
int a[100]; // array on stack (type a[100])
int *aa = new aa[100](0);
int *p = a; // array to pointer conversion
int b[100];
a = b; // compilation error
delete[] aa;
int *ptr = new int(10); // allocates int with value 10
delete ptr;
```
Подробнее про устройство памяти можно почитать [тут](https://craftofcoding.wordpress.com/2015/12/07/memory-in-c-the-stack-the-heap-and-static/)
# Указатели
Любой программный объект занимает некоторое место в памяти, его местоположение называется указателем.
```cpp=
int x;
int* p = &x; // & return x address
*p = 10; // * return value by pointer

&: T -> T* 
*: T* -> T // for nullptr UB
```
## Арифметика с указателями
```cpp=
+: (T*, int) -> T*   // a[10], (a+5) -> a[5] // actually a + x * sizeof(T)
+: (int, T*) -> T*   // a[10], (5+a) -> a[5]
-: (T*, int) -> T*   // a[10], (a[5] - 1) -> a[4]
-: (T*, T*)  -> int(int64_t)  // a[10], (a[5] - a[0]) -> 5 // actually (a[x] - a[y]) / sizeof(T)

// except for void
// except nullptr
```
Пустой указатель имеет тип `nullptr_t` и обозначается как `nullptr`.