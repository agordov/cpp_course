---
title: Структуры
tags: cpp, abstract, sem1
---


# Структуры
Для описания сложных типов данных, например, трехмерных векторов, атомов, и т.д. удобно объединять свойства этих типов в одно целое. Для этого в языке С были придуманы структуры, а С++ как язык, содержащий в себе С получил их в наследство. Изначально структуры имели ограниченный функционал, но позже были расширены. Пока речь будет идти о структурах как в языке С с некоторыми дополнениями. 

## Объявление структур
Посмотрим на синтаксис объявления структуры.

Синтаксис объявления структуры:
```cpp=
struct [structure tag] {
   member definition;
   member definition;
   ...
   member definition;
} [one or more structure variables];  
```
В примере выше structure tag является опциональным. Рассмотрим два примера:
<table>
<tr>
<th> Объявление типа Book и переменной book </th>
<th> Объявление одной переменной book </th>
</tr>
<tr>
<td>

```cpp=
struct Books {
    char  title[50];
    char  author[50];
    char  subject[100];
    int   book_id;
} book;
```

</td>
<td>

```cpp=
struct {
   char  title[50];
   char  author[50];
   char  subject[100];
   int   book_id;
} book;
```

</td>
</tr>
</table>

Т.е. в примере слева сразу объявляется тип Book и создается переменная такого типа, а значит в дальнейшем можно снова объявлять другие переменные типа Book. В примере справа же возможна только одна переменная такого типа.
## Доступ к полям структур
Структуры такого вида называются [POD](https://en.wikipedia.org/wiki/Passive_data_structure) (plain old data), для них доступ к полям всегда виден всем. И получить доступ к полю можно с помощью оператора `.`

Пример получения доступа к полям структур:
```cpp=
#include <iostream>

struct Point {
    float x, y;
};

int main() {
    Point p;
    p.x = 3.14f;
    p.y = 2.71f;
    std::cout << p.x << " " << p.y << std::endl;
}
```
Таким образом с помощью оператора `.` можно как получить значение поля, так и установить его.

## Передача структур в функции

Структуры как и многие другие типы данных могут быть переданы в функции. Так же существует три способа передачи в функцию - по значению, по ссылке, по указателю
<table>
<tr>
<th> По значению </th>
<th> По ссылке </th>
<th> По указателю </th>
</tr>
<tr>
<td>

```cpp=
struct A {
    int x;
};

void foo([struct] A var) {
    var.x = 10; // doesn't actually change value
}
```

</td>
<td>

```cpp=
struct A {
    int x;
};

void foo([struct] A &var) {
    var.x = 10; // actually change value
}
```

</td>
<td>
    
```cpp=
struct A {
    int x;
};

void foo([struct] A *var) {
    (*var).x = 10; // actually change value
    var->x = 10;
}
```
    
</td>
</tr>
</table>


В первом примере не произойдет изменение данных, в двух других произойдет. Кроме того, пример с указателем вводит новый оператор $->$, он сочетает в себе разыменование и доступ к полю. 
## Объединения. Unions
Специальный тип данных, который в отличие от структур позволяет хранить в себе различные типы данных в одной выделенной ячейке памяти.

Объявляются объединения как структуры.
<table>
<tr>
<th> Синтаксис объявления объединения </th>
<th> Пример объявления объединения </th>
</tr>
<tr>
<td>

```cpp=
union [union tag] {
   member definition;
   member definition;
   ...
   member definition;
} [one or more union variables];
```

</td>
<td>

```cpp=
union Data {
   int i;
   float f;
   char str[20];
} data;
```

</td>
</tr>
</table>

В памяти это выглядит следующим образом.
|Представление *union* в памяти|
|:-:|
|![](https://i.imgur.com/9y5uoGV.png)|

Пример доступа к полям объединения:
```cpp=
#include <iostream>
 
union Data {
   int i;
   float f;
   char str[20];
};
 
int main( ) {

   union Data data;        

   data.i = 10;
   data.f = 220.5;
   strcpy( data.str, "C Programming"); // copy chars into destination pointer 
   std::cout << data.i << '\n';
   std::cout << data.f << '\n';
   std::cout << data.str << '\n';

   return 0;
}
```
Вывод программы:
```cpp=
data.i : 1917853763
data.f : 4122360580327794860452759994368.000000
data.str : C Programming
```
Как видно из примера, данные перезаписали одну и ту же ячейку данных, в итоге значения в полях `i, f` оказались неверными.
## Перечисления. Enum/enum class
Для замены магических констант и повышения читаемости кода можно использовать перечисления.
<table>
<tr>
<th> Синтаксис перечислений </th>
<th> Пример объявления перечислений </th>
</tr>
<tr>
<td>

```cpp=
enum [optional tag] {
    name1,
    name2,
    name3
};
```

</td>
<td>

```cpp=
enum {
    name1,
    name2,
    name3
} someCodes;

someCodes otherCodes; // ERROR!

enum Codes {
    code1,
    code2,
    code3
};

Codes codes; // OK
```

</td>
</tr>
</table>

Перечисления могут содержать в себе только **целочисленные** значения, т.е. `int, unsigned` и т.д...
По умолчанию значения перечислений начинается с 0 и увеличивается на 1. Можно также задавать значения вручную. 

Пример задания пользовательских значений:
```cpp=
enum Codes {
    success = 0,
    anotherSuccess, // 1
    fail = -1,
    bigFail = fail - 100,
    
};
```
Кроме задания вручную значений можно задавать явно тип перечислений (обязательно целочисленный).

Пример указания типа перечисления:
```cpp=
enum Fruits : int {
    apple = 0,
    orange = 10,
    banana // 11
};
```
### Проблемы с перечислениями
Первая проблема, возникающая с перечислениями заключается в том, что перечисления становятся доступны в той области видимости, где были объявлены без дополнительных действий. Вторая проблема в том, что перечисления могут быть неявно приведены к другим типам.
<table>
<tr>
<th> Пример доступа к полям перечислений </th>
<th> Пример приведения типов для перечислений </th>
</tr>
<tr>
<td>

```cpp=
void f() {
    enum Fruits {
        apple, orange, banana
    };
    Fruits fruit = apple; // OK
    Fruits fruit = Fruits::apple; // OK
}
```

</td>
<td>

```cpp=
void f(int x) {
    enum Fruits {
        apple, orange, banana
    };
    Fruits fruit = apple; // OK
    Fruits fruit = Fruits::apple; // OK
    if (x == orange) { // bad codestyle, implicit type conversion
        // do smth
    }
}
```

</td>
</tr>
</table>

## Enum class
Для избежания вышеописанных проблем следует использовать `Enum class`. В этом случае не происходит неявного приведения типов и кроме того, перечесления не забивают область видимости.

Сравнение Enum и Enum class:
```cpp=
enum Color { red, green, blue };                    // plain enum 
enum Card { red_card, green_card, yellow_card };    // another plain enum 
enum class Animal { dog, deer, cat, bird, human };  // enum class
enum class Mammal { kangaroo, deer, human };        // another enum class

void fun() {

    // examples of bad use of plain enums:
    Color color = Color::red;
    Card card = Card::green_card;

    int num = color;    // no problem

    if (color == Card::red_card) // no problem (bad)
        cout << "bad" << endl;

    if (card == Color::green)   // no problem (bad)
        cout << "bad" << endl;

    // examples of good use of enum classes (safe)
    Animal a = Animal::deer;
    Mammal m = Mammal::deer;

    int num2 = a;   // error
    if (m == a)         // error (good)
        cout << "bad" << endl;

    if (a == Mammal::deer) // error (good)
        cout << "bad" << endl;

}
```