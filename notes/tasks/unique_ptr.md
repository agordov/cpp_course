---
title: unique_ptr
tags: tasks, 2sem
---

# Задание №1

Исправьте ошибки в примере реализации `Unique_ptr` и реализуйте недостающие методы. (можно посмотреть на стандартную реализацию [тут](https://en.cppreference.com/w/cpp/memory/unique_ptr))

```cpp=
template<typename T>
class Unique_ptr {
    T*   data;
public:
    Unique_ptr();
    Unique_ptr(T*);
    Unique_ptr(const Unique_ptr&);
    Unique_ptr(Unique_ptr&&);
    Unique_ptr& operator=(const Unique_ptr&);
    Unique_ptr& operator=(const Unique_ptr&&);
    ~Unique_ptr();
    T* operator->();
    T& operator*();
    T* release();
    void reset(T*);
    operator bool();
};
```

## Дополнительно

Реализуйте возможность добавления пользовательской функции освобождения ресурсов. Т.е. следующий код должен хотя бы частично работать.

```cpp=
int* f_creater() {
    int *res = new int;
    return res;
}

void f_deleter(int *ptr) {
    delete ptr;
}

int main() {
    {
        MyDeleter d;
        Unique_ptr<int, MyDeleter> ptr(new int, d);
    }
    {
        Unique_ptr<int, MyDeleter> ptr(new int, MyDeleter());
    }
    {
        Unique_ptr<int, MyDeleter> ptr;
    }
    {
        Unique_ptr<int, void(*)(int*)> ptr(f_creater(), f_deleter);
    }
}
```