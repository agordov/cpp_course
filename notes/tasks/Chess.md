---
title: Chess
tags: tasks, 2sem
---

# Задание

Для игры в шахматы был разработан следующий подход. В качестве базового класса был написан базовый класс "фигура", содержащий в себе клетку с полем. Вам требуется описать фигуры на основе этого класса с использованием наследования и виртуальных функций. Класс ячейки поля представляет собой набор двух координат.

```cpp=
class Piece {
protected:
    Cell c;
public:
    Piece(): c() {}
    Piece(Cell c): c(c) {}
    virtual ~Piece() {}
    
    // return current position
    virtual Cell position() const {
        return c;
    }
    
    virtual bool available(const Cell &c) const = 0;
};

class King;

class Pawn;

class Queen;

class Knight;

class Rook;

class Bishop;
```