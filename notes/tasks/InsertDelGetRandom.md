---
title: InsertDelGetRandom
tags: tasks, 2sem
---

# RandomizedSet

Реализуйте класс `RandomizedSet`:

* `RandomizedSet()` - конструктор по умолчанию.
* bool insert(int val) - добавляет значение в множество, если его было. Возвращает `true`, если удалось добавить значение, иначе `false`.
* bool remove(int val) - удаляет значение `val` из множества, если оно там было. Возвращает `true`, если удалось удалить, иначе `false`.
* int getRandom() - возвращает случайный элемент из множества (предполагается, что метод будет вызван когда множество не будет пустым). Получение любого элемента равновероятно. 

**Пример 1:**
```
Input
["RandomizedSet", "insert", "remove", "insert", "getRandom", "remove", "insert", "getRandom"]
[[], [1], [2], [2], [], [1], [2], []]
```
```
Output
[null, true, false, true, 2, true, false, 2]
```
**Пояснение:**
```cpp=
RandomizedSet randomizedSet = new RandomizedSet();
randomizedSet.insert(1); // Inserts 1 to the set. Returns true as 1 was inserted successfully.
randomizedSet.remove(2); // Returns false as 2 does not exist in the set.
randomizedSet.insert(2); // Inserts 2 to the set, returns true. Set now contains [1,2].
randomizedSet.getRandom(); // getRandom() should return either 1 or 2 randomly.
randomizedSet.remove(1); // Removes 1 from the set, returns true. Set now contains [2].
randomizedSet.insert(2); // 2 was already in the set, so return false.
randomizedSet.getRandom(); // Since 2 is the only number in the set, getRandom() will always return 2.
```

**Заготовка:**
```cpp=
class RandomizedSet {
public:
    RandomizedSet();
    
    bool insert(int val);
    
    bool remove(int val);
    
    int getRandom();

private:
};
```