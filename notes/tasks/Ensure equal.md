---
title: ensure equal
tags: tasks, 2sem
---

# Функция EnsureEqual

Напишите функцию
```cpp=
void EnsureEqual(const string& left, const string& right);
```
В случае, если строка **left** не равна строке **right**, функция должна выбрасывать исключение `runtime_error` с содержанием " != ", где и - строки, которые хранятся в переменных **left** и **right** соответственно. Обратите внимание, что вокруг знака неравенства в строке, которая помещается в исключение, должно быть ровно по одному пробелу.

Если `left == right`, функция должна корректно завершаться.

Например, код
```cpp=
int main() {
  try {
    EnsureEqual("Hello", "Hello");
    EnsureEqual("Hello", "World");
  } catch (runtime_error& e) {
    cout << e.what() << endl;
  }
  return 0;
}
```
должен выводить
```=
Hello != World
```