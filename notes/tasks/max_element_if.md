---
title: max_element_if
tags: tasks, 2sem
---

# Найти максимум

Реализуйте шаблонную функцию
```cpp=
template<typename ForwardIterator, typename UnaryPredicate>
ForwardIterator max_element_if(
  ForwardIterator first, ForwardIterator last, UnaryPredicate pred);
```
возвращающую итератор на максимальный элемент в диапазоне `first, last`, для которого предикат `pred` возвращает `true`. Если диапазон содержит несколько подходящих элементов с максимальным значением, то результатом будет первое вхождение такого элемента. Если диапазон не содержит подходящих элементов, то функция должна вернуть `last`.

**Пояснения**
* `pred` принимает аргумент того типа, который имеют элементы диапазона
* для `ForwardIterator` определены операторы `++, ==, !=, * (разыменование)`
* для типа, на который указывает итератор, определён оператор `<` («меньше»)

Для тестов можете использовать следующий код:
```cpp=
#include <string>
#include <vector>
#include <list>
#include <forward_list>
#include <numeric>
#include <iterator>
#include <algorithm>

void TestUniqueMax()
{
    auto IsEven = [](int x) 
    {
        return x % 2 == 0;
    };

    const std::list<int> hill{ 2, 4, 8, 9, 6, 4, 2 };
    auto max_iterator = hill.begin();
    std::advance(max_iterator, 2);

    std::vector<int> numbers(10);
    std::iota(numbers.begin(), numbers.end(), 1);

    // check if 
    // max_element_if(numbers.begin(), numbers.end(), IsEven) == --numbers.end()

    // check if
    // max_element_if(hill.begin(), hill.end(), IsEven) == max_iterator
}

void TestSeveralMax() 
{
    struct IsCapitalized 
    {
        bool operator()(const std::string& s)
        {
            return !s.empty() && std::isupper(s.front());
        }
    };

    const std::forward_list<std::string> text
    { "One", "two", "Three", 
      "One", "Two", "Three", 
      "one", "Two", "three" 
    };
    auto max_iterator = text.begin();
    std::advance(max_iterator, 4);

    // check if 
    // max_element_if(text.begin(), text.end(), IsCapitalized()) == max_iterator
}

void TestNoMax() 
{
    const std::vector<int> empty;
    const std::string str = "Non-empty string";

    auto AlwaysTrue = [](int) 
    {
        return true;
    };
    // check if
    // max_element_if(empty.begin(), empty.end(), AlwaysTrue) == empty.end()

    auto AlwaysFalse = [](char)
    {
        return false;
    };
    // check if 
    // max_element_if(str.begin(), str.end(), AlwaysFalse) == str.end() 
}
```

