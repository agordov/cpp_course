---
title: Считалка Иосифа
tags: tasks, 2sem
---

# Считалка Иосифа

Дан диапазон объектов некоторого типа. Напишите функцию, переставляющую его элементы в соответствии с порядком, определённым так называемой [считалкой Иосифа](https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0_%D0%98%D0%BE%D1%81%D0%B8%D1%84%D0%B0_%D0%A4%D0%BB%D0%B0%D0%B2%D0%B8%D1%8F) с заданным размером шага:
```cpp=
template <typename RandomIt>
void MakeJosephusPermutation(
    RandomIt range_begin, RandomIt range_end,
    uint32_t step_size
);
```
Гарантируется, что итераторы `range_begin` и `range_end` являются итераторами произвольного доступа, то есть допускают вычитание одного из другого и сложение с числом. Кроме того, вы можете полагаться на то, что `step_size > 0`. Тип переупорядочиваемых объектов можно получить с помощью выражения `typename RandomIt::value_type`. Объекты этого типа запрещено копировать. При наличии копирования этих объектов вы получите ошибку компиляции.

**Решение с копированиями**
Вам дано решение данной задачи, копирующее элементы и не эффективное по времени, но в остальном корректное. Вы можете исправить его или написать своё.

```cpp=
#include <cstdint>
#include <iterator>
#include <numeric>
#include <vector>
#include <iostream>

using namespace std;

template <typename RandomIt>
void MakeJosephusPermutation(RandomIt first, RandomIt last, uint32_t step_size) {
  std::vector<typename RandomIt::value_type> pool(first, last);
  size_t cur_pos = 0;
  while (!pool.empty()) {
    *(first++) = pool[cur_pos];
    pool.erase(pool.begin() + cur_pos);
    if (pool.empty()) {
      break;
    }
    cur_pos = (cur_pos + step_size - 1) % pool.size();
  }
}

std::vector<int> MakeTestVector() {
  std::vector<int> numbers(10);
  std::iota(std::begin(numbers), std::end(numbers), 0);
  return numbers;
}

void TestIntVector() {
  const std::vector<int> numbers = MakeTestVector();
  {
    std::vector<int> numbers_copy = numbers;
    MakeJosephusPermutation(std::begin(numbers_copy), std::end(numbers_copy), 1);
    // check if 
    // numbers_copy == numbers
  }
  {
    std::vector<int> numbers_copy = numbers;
    MakeJosephusPermutation(std::begin(numbers_copy), std::end(numbers_copy), 3);
    // check if 
    // numbers_copy == vector<int>({0, 3, 6, 9, 4, 8, 5, 2, 7, 1})
  }
}

// Это специальный тип, который поможет убедиться, что реализация
// функции MakeJosephusPermutation не выполняет копирование объектов.

struct NoncopyableInt {
  int value;

  NoncopyableInt(int value) : value(value) {}

  NoncopyableInt(const NoncopyableInt&) = delete;
  NoncopyableInt& operator=(const NoncopyableInt&) = delete;

  NoncopyableInt(NoncopyableInt&&) = default;
  NoncopyableInt& operator=(NoncopyableInt&&) = default;
};

bool operator == (const NoncopyableInt& lhs, const NoncopyableInt& rhs) {
  return lhs.value == rhs.value;
}

std::ostream& operator << (std::ostream& os, const NoncopyableInt& v) {
  return os << v.value;
}

void TestAvoidsCopying() {
  std::vector<NoncopyableInt> numbers;
  numbers.push_back({1});
  numbers.push_back({2});
  numbers.push_back({3});
  numbers.push_back({4});
  numbers.push_back({5});

  MakeJosephusPermutation(std::begin(numbers), std::end(numbers), 2);

  std::vector<NoncopyableInt> expected;
  expected.push_back({1});
  expected.push_back({3});
  expected.push_back({5});
  expected.push_back({4});
  expected.push_back({2});

  // check if 
  // numbers == expected
}
```

