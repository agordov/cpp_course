---
title: Rabbits
tags: tasks, 2sem
---

# Сколько было кроликов?

Есть лес с неизвестным количеством кроликов. Мы опросили **n** кроликов: *«Сколько кроликов такого же цвета, как вы?»* и собрали ответы в целочисленный массив ответов, где `answers[i]` — это ответ `i`-го кролика. Считается, что все кролики знают друг друга и говорят всегда правду.

Учитывая массив ответов, вычислите минимальное количество кроликов, которое может быть в лесу.

**Пример 1:**
```
Input: answers = [1,1,2]
```
```
Output: 5
```
**Пояснение:**
Два кролика, ответившие «1», могут быть одного цвета, например, красного.
Кролик, ответивший «2», не может быть красным, иначе ответы будут ложными (а кролики не врут).
Например, пусть кролик, ответивший «2», был синим.
Тогда в лесу должно быть еще 2 синих кролика, которые не ответили на вопрос.
Таким образом, наименьшее возможное количество кроликов в лесу равно 5: 3 ответивших плюс 2 не ответивших.

**Пример 2:**
```
Input: answers = [10,10,10]
```
```
Output: 11
```

**Заготовка решения**
```cpp=
int numRabbits(vector<int>& answers) {
    // your code        
}
```