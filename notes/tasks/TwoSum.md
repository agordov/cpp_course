---
title: TwoSum
tags: tasks, 2sem
---

# Сумма двух

Дан массив целых чисел `nums` и целое число `target`. Верните пару индексов, которые будут в сумме давать число `target`.

Предполагается, что решение всегда существует, и нельзя использовать один элемент дважды.

**Пример 1:**
```
Input: nums = [2,7,11,15], target = 9
```
```
Output: [0,1]
```

**Пример 2:**
```
Input: nums = [3,2,4], target = 6
```
```
Output: [1,2]
```
**Пример 3:**
```
Input: nums = [3,3], target = 6
```
```
Output: [0,1]
```

**Заготовка:**
```cpp=
vector<int> twoSum(vector<int>& nums, int target) {
   // your code     
}
```