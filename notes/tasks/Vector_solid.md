---
title: Vector SOLID
tags: tasks, 2sem
---

# Задача №2. Вектор и ко

В базовой версии задачи №2 класс вектор самостоятельно занимался выделением памяти и организацией доступа к данным. Согласно принципам SOLID хорошо было бы разделить эти обязанности. Для этого необходимо реализовать некоторый класс `MyVectorBuffer`, который будет заниматься выделением памяти и её освобождением, а сам класс `MyVector` будет являться оберткой над ним.

```cpp=
template <typename T> void construct(T *p, const T &rhs); // creates object T in raw data

template <class T> void destroy(T *p) noexcept; // destroys object T by pointer

template <typename FwdIter> void destroy(FwdIter first, FwdIter last) noexcept;
    // destroys objects from to

template <typename T> struct MyVectorBuffer {
protected:
    T *arr_;
    size_t size_, used_ = 0;

protected:
    MyVectorBuf(const MyVectorBuf &) = delete;
    MyVectorBuf &operator=(const MyVectorBuf &) = delete;
    MyVectorBuf(MyVectorBuf &&rhs) noexcept;

    MyVectorBuf &operator=(MyVectorBuf &&rhs) noexcept;

    MyVectorBuf(size_t sz = 0);

    ~MyVectorBuf();
};

template <typename T> struct MyVector : private MyVectorBuffer<T> {
    using MyVectorBuf<T>::arr_;
    using MyVectorBuf<T>::size_;
    using MyVectorBuf<T>::used_;

    explicit MyVector(size_t sz = 0);

    MyVector(MyVector &&rhs) = default;

    MyVector &operator=(MyVector &&rhs) = default;

    MyVector(const MyVector &rhs);

    MyVector &operator=(const MyVector &rhs);

    T top() const;

    void pop();

    void push(const T &t);

    size_t size() const;
    size_t capacity() const;
};

```