---
title: Vector
tags: tasks, 2sem
---

# Задача №2. Вектор

В стандартной библиотеке существует последовательный шаблонный контейнер `vector<>`. Его идея заключается в том, что память под данные размещается последовательно и при необходимости перевыделяется.

Простая незащищенная версия может выглядеть следующим образом:

```cpp=
template <typename T>
class MyVector {
    T *arr_ = nullptr;
    size_t size_, used_ = 0;

public:
    explicit MyVector(size_t sz = 0) : arr_(new T[sz]), size_(sz) {}

    MyVector(const MyVector &rhs)
      : arr_(new T[rhs.size_]), size_(rhs.size_), used_(rhs.used_) {
        for (size_t idx = 0; idx != size_; ++idx)
          arr_[idx] = rhs.arr_[idx];
    }

    MyVector(MyVector &&rhs) noexcept
      : arr_(rhs.arr_), size_(rhs.size_), used_(rhs.used_) {
        rhs.arr_ = nullptr;
        rhs.size_ = 0;
        rhs.used_ = 0;
    }

    MyVector &operator=(MyVector &&rhs) noexcept {
        std::swap(arr_, rhs.arr_);
        std::swap(size_, rhs.size_);
        std::swap(used_, rhs.used_);
    }

    MyVector &operator=(const MyVector &rhs) {
        if (this != &rhs) {
          size_ = rhs.size_;
          used_ = rhs.used_;
          delete[] arr_;
          arr_ = new T[size_];
          for (size_t idx = 0; idx != size_; ++idx)
            arr_[idx] = rhs.arr_[idx];
    }
        return *this;
    }

    ~MyVector() { delete[] arr_; }

    T pop() { // removes the top element and returns it
        if (used_ < 1)
          throw std::runtime_error("Vector is empty");
        used_ -= 1;
        return arr_[used_];
    }

    void push(const T &t) {
        if (used_ == size_) {
          std::cout << "Realloc\n";
          size_t newsz = size_ * 2 + 1;
          T *newarr = new T[newsz];
          for (size_t idx = 0; idx != size_; ++idx)
            newarr[idx] = arr_[idx];
          delete[] arr_;
          arr_ = newarr;
          size_ = newsz;
        }
        arr_[used_] = t;
        ++used_;
    }

    size_t size() const { return used_; }
    size_t capacity() const { return size_; }
};
```

С этим классом много проблем. Как логических, так и в плане безопасности исключений. Перепишите его так, чтобы избавиться от большинства возникших проблем.

Для начала напишите безопасную функцию копирования. Она должна выделять память, затем копировать поэлементно все поданные данные. При возникновении исключения выделенная память должна быть освобождена, а исключение выброшено наружу.

```cpp=
template <typename T>
T *safe_copy(const T *src, size_t srcsize, size_t dstsize) {
    // some code
}
```

Заготовка безопасного класса

```cpp=
template <typename T>
class MyVector {
    T *arr_ = nullptr;
    size_t size_, used_ = 0;
    
public:
    explicit MyVector(size_t sz = 0);
    MyVector(const MyVector &rhs);
    MyVector(MyVector &&rhs) noexcept;
    MyVector &operator=(MyVector &&rhs) noexcept;   
    MyVector &operator=(const MyVector &rhs);
    ~MyVector();
    void pop(); // remove the top elemtn, throw if no element
    T& top(); // get the top element safely, throw if no element
    void push(const T &t);
    T& at(size_t idx); // safely get the idx's element, throw if out of range
    T& operator[](size_t idx); // unsafely get the idx's element
    size_t size() const;
    size_t capacity() const;
};
```

Для проверки используйте следующий класс:
```cpp=
struct Controllable {
    static int control;
    int *resource_;
    Controllable() : resource_(new int(42)) {}

    Controllable(Controllable &&rhs) noexcept : resource_(rhs.resource_) {
        rhs.resource_ = nullptr;
    }
    Controllable &operator=(Controllable &&rhs) noexcept {
        std::swap(resource_, rhs.resource_);
        return *this;
    }
    Controllable(const Controllable &rhs) : resource_(new int(*rhs.resource_)) {
        if (control == 0) {
          control = 5;
          std::cout << "Control reached\n";
          throw std::bad_alloc{};
    }
        control -= 1;
    }
    Controllable &operator=(const Controllable &rhs) {
        Controllable tmp(rhs);
        std::swap(*this, tmp);
        return *this;
    }

    ~Controllable() { delete resource_; }
};
```