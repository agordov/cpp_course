/*
*
* Based on the Scott Meyers article Dimensional Analysis in C++.
*
*/

#include <iostream>
#include "utils.hpp"

template<int m = 0, int kg = 0, int s = 0, int A = 0, int K = 0, int mol = 0, int cd = 0>
using Dimension = IntList<m, kg, s, A, K, mol, cd>;

template<typename Dim>
class Quantity {
    double value_;
    
public:
    explicit Quantity(double value = 0.0): value_(value) {}
    Quantity(const Quantity& other): value_(other.value_) {}
    Quantity& operator=(const Quantity& other) {
        value_ = other.value_;
    }
    double value() const noexcept {
        return value_;
    }

    Quantity& operator+=(const Quantity& other) {
        value_ += other.value_;
        return *this;
    }
    
    Quantity& operator-=(const Quantity& other) {
        value_ -= other.value_;
        return *this;
    }

    Quantity& operator*=(double rhs) {
        value_ *= rhs;
        return *this;
    } 

    Quantity& operator/=(double rhs) {
        value_ /= rhs;
        return *this;
    } 
};

template<typename Dim>
Quantity<Dim> operator+(const Quantity<Dim>& lhs, const Quantity<Dim>& rhs) {
    Quantity<Dim> tmp = lhs;
    tmp += rhs;
    return tmp;
}

template<typename Dim>
Quantity<Dim> operator-(const Quantity<Dim>& lhs, const Quantity<Dim>& rhs) {
    Quantity<Dim> tmp = lhs;
    tmp -= rhs;
    return tmp;
}

template<typename Dim>
Quantity<Dim> operator*(double lhs, const Quantity<Dim>& rhs) {
    Quantity<Dim> tmp = rhs;
    return tmp *= lhs;
}

template<typename Dim>
Quantity<Dim> operator*(const Quantity<Dim>& lhs, double rhs) {
    Quantity<Dim> tmp = lhs;
    return tmp *= rhs;
}

template<typename Dim>
Quantity<Dim> operator/(const Quantity<Dim>& lhs, double rhs) {
    Quantity<Dim> tmp = lhs;
    return tmp /= rhs;
}

template<int a, int b>
struct Plus {
    static int const value = a + b;
};

template<int a, int b>
struct Minus {
    static int const value = a - b;
};

template<typename Dim>
auto operator/(double lhs, const Quantity<Dim>& rhs) ->
    decltype(Quantity< typename Zip<Dimension<>, Dim, Minus>::type>())
{
    return Quantity< typename Zip<Dimension<>, Dim, Minus>::type> (lhs / rhs.value());
}

template<typename Dim1, typename Dim2>
auto operator*(const Quantity<Dim1> &lhs, const Quantity<Dim2> &rhs) -> 
    decltype(Quantity< typename Zip<Dim1, Dim2, Plus>::type >())
{
    return Quantity< typename Zip<Dim1, Dim2, Plus>::type > (lhs.value() * rhs.value());
}

template<typename Dim1, typename Dim2>
auto operator/(const Quantity<Dim1> &lhs, const Quantity<Dim2> &rhs) -> 
    decltype(Quantity< typename Zip<Dim1, Dim2, Minus>::type >())
{
    return Quantity< typename Zip<Dim1, Dim2, Minus>::type > (lhs.value() / rhs.value());
}

using NumberQ   = Quantity<Dimension<>>;           // число без размерности
using LengthQ   = Quantity<Dimension<1>>;          // метры
using MassQ     = Quantity<Dimension<0, 1>>;       // килограммы
using TimeQ     = Quantity<Dimension<0, 0, 1>>;    // секунды
using VelocityQ = Quantity<Dimension<1, 0, -1>>;   // метры в секунду
using AccelQ    = Quantity<Dimension<1, 0, -2>>;   // ускорение, метры в секунду в квадрате
using ForceQ    = Quantity<Dimension<1, 1, -2>>;   // сила в ньютонах

int main() {
    NumberQ n {1.};
    MassQ m(100);
}
