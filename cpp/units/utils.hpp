#ifndef H_UNITUTILS
#define H_UNITUTILS

template<int ... List>
struct IntList;

template<int H, int... T>
struct IntList<H, T...> {
    static const int Head = H;
    using Tail = IntList<T...>;
};

template<>
struct IntList<> {};

template<int H, typename TL>
struct IntCons;

template<int H, int ... Values>
struct IntCons<H, IntList<Values...>> {
    using type = IntList<H, Values...>;
};

template<typename IL1, typename IL2, template<int, int> class F>
struct Zip {
    using type = typename IntCons
        <F<IL1::Head, IL2::Head>::value
        , typename Zip<typename IL1::Tail
        , typename IL2::Tail, F>::type>
        ::type;
};

template<template<int,int> class F>
struct Zip<IntList<>, IntList<>, F> {
    using type = IntList<>;
};
#endif