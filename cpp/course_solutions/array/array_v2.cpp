#include <cstddef>
#include <algorithm>

template <typename T>
class Array {
public:    

    explicit Array(size_t size_, const T& value)
        : size_(size_)
        , data(static_cast<T*>(operator new [](size_ * sizeof(T))))
    {
        for(size_t i = 0; i < size_; ++i) {
           new (data + i) T(value);
        }              
    }

    Array()
        : size_(0)
        , data(nullptr)
    {
    }
    
    Array(const Array &other)
        : size_(other.size_)
        , data(static_cast<T*>(operator new [](size_ * sizeof(T))))
    {
        for(size_t i = 0; i < size_; ++i) {
           new (data + i) T(other[i]);
        }   
    }
    
    Array(Array &&other) {
        swap(other);
    }
    // реализуйте перемещающий оператор присваивания
    Array& operator=(Array &&other) {
        swap(other);
        return *this;
    }

    ~Array() {
        for(size_t i = 0; i < size_; ++i) {
            data[i].~T();
        }
        operator delete [] (data);
    }
    
    Array& operator=(const Array &other) {
        Array tmp = other;
        std::swap(tmp.size_, size_);
        std::swap(tmp.data, data);
        return *this;
    }
    
    size_t size() const {
        return size_;
    }
    
    T& operator[](size_t idx) {
        return data[idx];
    }

    const T& operator[](size_t idx) const {
        return data[idx];
    }

private:

    void swap(Array &other) {
        std::swap(size_, other.size_);
        std::swap(data_, other.data_);
    }

    size_t size_;
    T *data;
};