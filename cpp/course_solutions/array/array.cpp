#include <cstddef>
#include <algorithm>

// T required to satisfy T(), copy_assignment

template <typename T>
class Array {
public: 

    explicit Array(size_t size_ = 0, const T& value = T())
        : size_(size_)
        , data(new T[size_]) 
    {
        for(size_t i = 0; i < size_; ++i) {
            data[i] = value;
        }
    }

    Array(const Array &other) {
        size_ = other.size_;
        data = new T[size_];
        std::copy(other.data, other.data + size_, data);
    }

    ~Array() {
        delete []data;
    }

    Array& operator=(const Array &other) {
        Array tmp = other;
        std::swap(tmp.size_, size_);
        std::swap(tmp.data, data);
        return *this;
    }

    size_t size() const {
        return size_;
    }

    T& operator[](size_t idx) {
        return data[idx];
    }
    const T& operator[](size_t idx) const {
        return data[idx];
    }

private:
    size_t size_;
    T *data;
};