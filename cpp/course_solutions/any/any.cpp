#include "cloneable.cpp"

class Any
{
public:

    Any()
        : value_(nullptr)
    {
    }

    template<typename T>
    Any(const T &value)
        : value_(new ValueHolder<T>(value))
    {
    }

    Any(const Any &other)
        : value_(other.value_ ? other.value_->clone(): nullptr)
    {
    }   
    
    ~Any() {
        clear();
    }
    
    Any& operator=(const Any &other) {
        if (this == &other) {
            return *this;
        }
        clear();
        value_ = other.value_ ? other.value_->clone(): nullptr;
        return *this;
    }
    
    template<typename T>
    Any& operator=(const T &value) {
        clear();
        value_ = new ValueHolder<T>(value);
        return *this;
    }
    
    template<typename T>
    T* cast() {
        auto retptr = dynamic_cast<ValueHolder<T>*>(value_);
        return retptr ? &(retptr->data_) : nullptr;
    }
    
private:
    
    void clear() {
        if (value_) {
            delete value_;
        }
    }
    
    ICloneable* value_;
};