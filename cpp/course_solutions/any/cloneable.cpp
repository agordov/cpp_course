struct ICloneable
{
	virtual ICloneable* clone() const = 0;
	virtual ~ICloneable() { }
};

template <typename T>
struct ValueHolder: virtual public ICloneable {

    ValueHolder(const T &data): data_(data) {
    }

    ICloneable* clone() const override {
        return new(operator new(sizeof(ValueHolder))) ValueHolder(data_);
    }

    ~ValueHolder() = default;
    
    T data_;
};