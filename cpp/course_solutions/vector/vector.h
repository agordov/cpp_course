#include <stdexcept>

#ifndef _H_MYVECTOR_
#define _H_MYVECTOR_

template <typename T>
T *safe_copy(const T *src, size_t srcsize, size_t dstsize) {
    assert(srcsize <= dstsize);
    T *dest = new T[dstsize];
    try {
        for (size_t i = 0; i != dstsize; ++i) {
            dest[i] = src[i];
        }
    }
    catch (...) {
        delete[] dest;
        throw;
    }
    return dest;
}

template <typename T>
class MyVector
{
    T *arr_ = nullptr;
    size_t size_, used_ = 0;

  public:
    explicit MyVector(size_t sz = 0)
        : arr_(new T[sz])
        , size_(sz) {}
    MyVector(const MyVector &rhs) {
        arr_  = safe_copy(rhs.arr_, rhs.used_, rhs.size_);
        used_ = rhs.used_;
        size_ = rhs.size_;
    }
    MyVector(MyVector &&rhs) noexcept
        : arr_(rhs.arr_)
        , size_(rhs.size_)
        , used_(rhs.used_) {
        rhs.arr_  = nullptr;
        rhs.size_ = 0;
        rhs.used_ = 0;
    }
    MyVector &operator=(MyVector &&rhs) noexcept {
        std::swap(arr_, rhs.arr_);
        std::swap(size_, rhs.size_);
        std::swap(used_, rhs.used_);
        return *this;
    }
    MyVector &operator=(const MyVector &rhs) {
        MyVector tmp(rhs);
        std::swap(arr_, tmp.arr_);
        std::swap(size_, tmp.size_);
        std::swap(used_, tmp.used_);
        return *this;
    }
    ~MyVector() {
        delete[] arr_;
    }
    void pop() {   // remove the top elemtn, throw if no element
        if (used_ < 1) {
            throw std::out_of_range();
        }
        --used_;
    }
    T &top() {   // get the top element safely, throw if no element
        if (used_ < 1) {
            throw std::out_of_range();
        }
        return arr_[used_ - 1];
    }
    void push(const T &t) {
        if (used_ == size_) {
            size_t new_size = size_ * 2;
            T *new_arr      = safe_copy(arr_, size_, new_size);
            delete[] arr_;
            arr_  = new_arr;
            size_ = new_size;
        }
        arr_[used_] = t;
        ++used_;
    }
    T &at(size_t idx) {   // safely get the idx's element, throw if out of range
        if (idx > used_) {
            throw std::out_of_range();
        }
        return arr_[idx];
    }
    T &operator[](size_t idx) {   // unsafely get the idx's element
        return arr_[idx];
    }
    T &operator[](size_t idx) const {
        return arr_[idx];
    }
    size_t size() const {
        return used_;
    }
    size_t capacity() const {
        return size_;
    }
};

#endif   // _H_MYVECTOR_
