#include <stdexcept>

#ifndef _H_MYVECTOR_
#define _H_MYVECTOR_

template <typename T>
void construct(T *p, const T &rhs) {   // creates object T in raw data
    new (p) T(rhs);
}

template <class T>
void destroy(T *p) noexcept {   // destroys object T by pointer
    p->~T();
}

template <typename FwdIter>
void destroy(FwdIter first, FwdIter last) noexcept {   // destroys objects from to
    for (; first != last; ++first) {
        destroy(&*first++);
    }
}

template <typename T>
struct MyVectorBuffer
{
  protected:
    T *arr_;
    size_t size_, used_ = 0;

  protected:
    MyVectorBuffer(const MyVectorBuffer &)            = delete;
    MyVectorBuffer &operator=(const MyVectorBuffer &) = delete;
    MyVectorBuffer(MyVectorBuffer &&rhs) noexcept
        : arr_(rhs.arr_)
        , size_(rhs.size_)
        , used_(rhs.used_) {
        rhs.arr_  = nullptr;
        rhs.size_ = 0;
        rhs.used_ = 0;
    }

    MyVectorBuffer &operator=(MyVectorBuffer &&rhs) noexcept {
        std::swap(arr_, rhs.arr_);
        std::swap(size_, rhs.size_);
        std::swap(used_, rhs.used_);
        return *this;
    }

    MyVectorBuffer(size_t sz = 0)
        : arr_(sz == 0 ? nullptr : static_cast<T *>(::operator new(sizeof(T) * sz)))
        , size_(sz) {}

    ~MyVectorBuffer() {
        destroy(arr_, arr_ + used_);
        ::operator delete(arr_);
    }
};

template <typename T>
struct MyVector : private MyVectorBuffer<T>
{
    using MyVectorBuffer<T>::arr_;
    using MyVectorBuffer<T>::size_;
    using MyVectorBuffer<T>::used_;

    explicit MyVector(size_t sz = 0)
        : MyVectorBuffer<T>(sz) {}

    MyVector(MyVector &&rhs) = default;

    MyVector &operator=(MyVector &&rhs) = default;

    MyVector(const MyVector &rhs)
        : MyVectorBuffer<T>(rhs.used_) {
        while (used_ < rhs.used_) {
            construct(arr_ + used_, rhs.arr_[used_++]);
        }
    }

    MyVector &operator=(const MyVector &rhs) {
        MyVector tmp(rhs);
        std::swap(*this, tmp);
        return *this;
    }

    void pop() {   // remove the top elemtn, throw if no element
        if (used_ < 1) {
            throw std::out_of_range();
        }
        --used_;
        destroy(arr_ + used_);
    }
    T &top() {   // get the top element safely, throw if no element
        if (used_ < 1) {
            throw std::out_of_range();
        }
        return arr_[used_ - 1];
    }
    void push(const T &t) {
        if (used_ == size_) {
            MyVector tmp(size_ * 2);
            while (tmp.used_ < used_) {
                tmp.push(arr_[tmp.used_]);
            }
            tmp.push(t);
            std::swap(*this, tmp);
            return;
        }
        construct(arr_ + used_, t);
        ++used_;
    }

    T &at(size_t idx) {   // safely get the idx's element, throw if out of range
        if (idx > used_) {
            throw std::out_of_range();
        }
        return arr_[idx];
    }

    T &operator[](size_t idx) {   // unsafely get the idx's element
        return arr_[idx];
    }
    T &operator[](size_t idx) const {   // unsafely get the idx's element
        return arr_[idx];
    }
    size_t size() const {
        return used_;
    }

    size_t capacity() const {
        return size_;
    }
};

#endif   // _H_MYVECTOR_
