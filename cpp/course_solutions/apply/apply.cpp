#include <utility> // std::move && std::forward

template<typename Func, typename... Args>
auto apply (Func f, Args&&... args) -> decltype(f(std::forward<Args>(args)...))
{
    return f(std::forward<Args>(args)...);
}