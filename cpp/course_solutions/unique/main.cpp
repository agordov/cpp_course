#include <iostream>

#include "unique.h"

int main() {
    UP::Unique_ptr<int> a(new int(42));

    UP::Unique_ptr<int> b;

    b = UP::make_unique<int>(2);

    return 0;
}