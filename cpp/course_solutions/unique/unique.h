#ifndef _H_UNIQUE_
#define _H_UNIQUE_

#include <type_traits>
namespace UP {

    template <typename T>
    struct default_delete
    {
        void operator()(T *ptr) const {
            delete ptr;
        }
    };

    template <typename T>
    struct default_delete<T[]>
    {
        void operator()(T *ptr) const {
            delete[] ptr;
        }
    };

    template <typename T, typename D>
    class Unique_ptr
    {
        T *data;

      public:
        Unique_ptr()
            : data(nullptr) {}
        explicit Unique_ptr(T *ptr)
            : data(ptr) {}
        Unique_ptr(std::nullptr_t)
            : data(nullptr) {}

        Unique_ptr(Unique_ptr<T> &&other)
            : data(other.data) {
            other.data = nullptr;
        }
        Unique_ptr &operator=(Unique_ptr<T> &&other) noexcept {
            reset(other.release());
            return *this;
        }
        Unique_ptr(const Unique_ptr<T> &)            = delete;
        Unique_ptr &operator=(const Unique_ptr<T> &) = delete;

        ~Unique_ptr() noexcept {
            reset();
        }
        T *operator->() const {
            return data;
        }
        T &operator*() const {
            return *data;
        }
        T *get() const {
            return data;
        }
        explicit operator bool() const {
            return data;
        }

        T *release() noexcept {
            T *res = nullptr;
            std::swap(res, data);
            return res;
        }
        void reset(T *ptr) {
            T *res = release();
            D()(res);
        }
        void reset() {
            D()(data);
        }
    };

    template <typename T, typename D>
    class Unique_ptr<T[], D>
    {
        T *data;

      public:
        Unique_ptr()
            : data(nullptr) {}
        explicit Unique_ptr(T *ptr)
            : data(ptr) {}
        Unique_ptr(std::nullptr_t)
            : data(nullptr) {}

        Unique_ptr(Unique_ptr<T[]> &&other)
            : data(other.data) {
            other.data = nullptr;
        }
        Unique_ptr &operator=(Unique_ptr<T[]> &&other) noexcept {
            reset(other.release());
            return *this;
        }
        Unique_ptr(const Unique_ptr<T[]> &)            = delete;
        Unique_ptr &operator=(const Unique_ptr<T[]> &) = delete;

        ~Unique_ptr() noexcept {
            reset();
        }
        T &operator[](std::size_t idx) const {
            return data[idx];
        }
        T *get() const {
            return data;
        }
        explicit operator bool() const {
            return data;
        }

        T *release() noexcept {
            T *res = nullptr;
            std::swap(res, data);
            return res;
        }
        void reset(T *ptr) {
            T *res = release();
            D()(res);
        }
        void reset() {
            D()(res);
        }
    };

    template <class T, class... Args>
    std::enable_if_t<!std::is_array_v<T>, Unique_ptr<T, default_delete<T>>> make_unique(Args &&...args) {
        return Unique_ptr<T>(new T(std::forward<Args>(args)...));
    };

    template <class T>
    std::enable_if_t<std::is_array_v<T>, Unique_ptr<T, default_delete<T[]>>> make_unique(int size) {
        using type = std::remove_extent_t<T>;
        return Unique_ptr<T>(new type[size]);
    };
}   // namespace UP
#endif   // _H_UNIQUE_
