#include <string>
#include <sstream>
#include <exception>

class bad_from_string : public std::exception {
public:
    bad_from_string(std::string e ="from_string except") : info(e)
    {
    }
    const char* what() const noexcept {
        return info.c_str();
    }
private:
    std::string info;
};

template<typename T>
T from_string(std::string const& s) {

    std::istringstream is(s);
    T t;
    is >> std::noskipws >> t;
    if (is.fail()) 
        throw bad_from_string("conversion error");
    is.get(); 
    if(is.good()) 
        throw bad_from_string("conversion error");
    return t;   
}