---
title: Материалы курса по С++
tags: cpp
---

# Конспекты

## Часть №1

1. [Введение. Основы синтаксиса № 1](/notes/abstracts/Spring/Intro_1.md)
2. [Введение. Основы синтаксиса № 2](/notes/abstracts/Spring/Intro_2.md)
3. [Память](/notes/abstracts/Spring/Memory_layout.md)
4. [Массивы](/notes/abstracts/Spring/Arrays.md)
5. [Сортировки](/notes/abstracts/Spring/Sorts.md)
6. [Структуры](/notes/abstracts/Spring/Structs.md)
7. [Списки, стек, очередь](/notes/abstracts/Spring/Seq_Data_Structures.md)
8. [Бинарные деревья](/notes/abstracts/Spring/Binary_trees.md)
9. [Хеш-таблицы]()

## Часть №2

1. [Классы и структуры](notes/abstracts/Fall/OOP_basics_classes.md)
2. [Наследование](notes/abstracts/Fall/OOP_basics_inheritance.md)
3. [Операторы](notes/abstracts/Fall/OOP_basics_operators.md)
4. [RAII](notes/abstracts/Fall/RAII.md)
5. [Шаблоны](notes/abstracts/Fall/Templates.md)
6. [Исключения](notes/abstracts/Fall/Exceptions.md)
7. [Умные указатели](notes/abstracts/Fall/Smart_pointers.md)
8. [Итераторы](notes/abstracts/Fall/Iterators.md)
9. [Контейнеры](notes/abstracts/Fall/Containers_lambda.md)
10. [Тесты, CMake](notes/abstracts/Fall/Tests_cmake.md)

# Задачи

## Часть №1

1. [Динамика](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/SkWyGDY5o)
2. [Жадные алгоритмы](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/BkX5aDY5o)
3. [Обратная польская запись](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/HkiMsOKcs)
4. [Подготовка к кр](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/H1x755Y5o)
5. [Восхождения](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/ByBgrH5U9)
6. [Римские числа](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/ryHptr9L5)
7. [Наименьшая сумма](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/rkpPvrc8c)
8. [Xбоначчи](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/r18ufr9Iq)
9. [Факториальные числа](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/HyOIR4cU5)
10. [Детерминант](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/BJXWaN5Uc)
11. [Редакционное растояние](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/HyWFiQ9L9)
12. [Перемножение матриц](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/Skhdj7qI9)
13. [Спиральки](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/HkNusm9Lc)
14. [Упрощение](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/HkHtJLcIc)

## Часть №2

1. [Chess](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/Bk3oa3ZMj)

## Задачи на ввод/вывод

1. [Read & Print table](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/SkNvXhkri)
2. [Fixed precision](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/BJVf72JHj)
3. [Read file](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/ByaJGn1Hj)
4. [from_string](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/HkwiOoySo)
5. [Numfilter](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/HkKQpoJHs)

## Задачи на использование контейнеров

1. [Считалка Иосифа](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/BJN-GftHo)
2. [Contigious_array](https://hackmd.io/@zachet-cpp-mipt-2022/Sy9bzJTHs)
3. [Bipartition](https://hackmd.io/@zachet-cpp-mipt-2022/S115e1aSi)
4. [Repeat DNA](https://hackmd.io/@zachet-cpp-mipt-2022/HJ720C2ro)
5. [Group anagrams](https://hackmd.io/@zachet-cpp-mipt-2022/Sy_FhC3Hj)
6. [Majority elements](https://hackmd.io/@zachet-cpp-mipt-2022/Sy5RiA2Bi)
7. [Remove duplicates](https://hackmd.io/@zachet-cpp-mipt-2022/BJz6903Sj)
8. [Two Sum](https://hackmd.io/@zachet-cpp-mipt-2022/ByusFRhri)
8. [InsertDelGetRandom](https://hackmd.io/@zachet-cpp-mipt-2022/BJjx_C2So)
10. [Rabbits](https://hackmd.io/@zachet-cpp-mipt-2022/BJRH8AnBi)
 
## Задачи на исключения

1. [Ensure equal](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/Hk7_V3JHi)
2. [math_error](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/BkeNVY8Vo)

## Задачи на шаблоны

1. [apply](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/Sy2Vf867o)
2. [flatten](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/Syqj67N7s)

## Задачи на итераторы

1. [max_element_if](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/rkkG1fYrs)
2. [Sort Iterator](https://hackmd.io/@zachet-cpp-mipt-2022/B1IDQ17ro)

## Задачи на умные указатели

1. [shared_ptr](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/r1pXyPpQi)
2. [unique_ptr](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/ByRRASpmj)
3. [unique_ptr (array version)](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/BJU2HI6Xo)

## Задачи на создание контейнеров

1. [vector V1](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/ryUeF9LNj)
2. [vector V2](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/rk3XxiUVo)
3. [Array V1](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/B1lD9QEXs)
4. [Array V2](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/H16HhX4mj)

## Type erasure задачи

1. [value_holder](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/rkBRhmVXs)
2. [value_holder (easy version)](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/BJpDp7EXo)
3. [any](https://hackmd.io/@55rfhHjhQKCsHkjhAa1mrg/H1b2A7VXi)

# Основные ссылки/ресурсы для поиска ответов на вопросы

1. Документация по языку + примеры - https://en.cppreference.com/w/
2. Ответы на вопросы - https://stackoverflow.com/
3. Онлайн компилятор для быстрого просмотра и запуска кода - https://godbolt.org/
4. CMake документация - https://cmake.org/
5. Компилятор gcc - https://gcc.gnu.org/
6. Компилятор clang - https://clang.llvm.org/
7. Удобная IDE - https://www.jetbrains.com/clion/
8. Удобный редактор кода - [https://code.visualstudio.com/](https://www.vim.org/)
9. Гайд по отладке в VS Code - https://code.visualstudio.com/docs/cpp/cpp-debug
10. Как надо писать код на плюсах - https://github.com/isocpp/CppCoreGuidelines
